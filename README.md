# Ghost Runner
This is a game

### The Game
It's 2D Side View and multi-local (maybe online in the future) Runner.
All the monsters want to be the white rabbit but there is only one white rabbit, each monster can become the white rabbit.
The white rabbit will always wins at this end

### Technology
* [PixiJs](http://www.pixijs.com/) - For the graphics
* [Victor.js](http://victorjs.org/) - To handle the vectors
* [JavaScript ES6](http://es6-features.org/) - For anything else

### Assets
* [Monsters](https://graphicriver.net/item/8-scary-creatures-sprite-sheets/9538952)
* [Rabbit](https://graphicriver.net/item/jumping-game-character-rabbit-sprite-sheet/7736464)
* [Map Tileset & UI](http://www.gameart2d.com/)