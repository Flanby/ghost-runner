$(window).on("blur focus", function(e) {
    var prevType = $(this).data("prevType");

    if (prevType != e.type) {   //  reduce double fire issues
        switch (e.type) {
            case "blur":
                GameCore.setPause(true);
                break;
            case "focus":
                GameCore.setPause(false);
                break;
        }
    }

    $(this).data("prevType", e.type);
})


$(document).ready(function(){
	GameScene.onGameReady = function() {

		GameScene.addItem(new Player(5).setName("GamePad2", 0xff3300).setPos(300, 200).setAlive(false).setKeymap(Input.keymaps.Move.GamePad1));
		GameScene.addItem(new Player(4).setName("zqsd", 0xffff00).setPos(500, 75).setAlive(false).setKeymap(Input.keymaps.Move.ZQSD));
		GameScene.addItem(new Player(3).setName("GamePad1", 0x33cccc).setPos(261, 499).setAlive(false).setKeymap(Input.keymaps.Move.GamePad1));
		GameScene.addItem(new Player(2).setName("8456", 0xff99ff).setPos(175, 75).setAlive(false).setKeymap(Input.keymaps.Move.Pad));
		GameScene.addItem(new Player(1).setName("Main", 0x66ff33).setPos(400, 300));

		mapGenerator.genMap(25).forEach(function(element) {
			GameScene.addItem(element);
		});
	};
	
	new FontLoader(["MotionControl-Bold"], {
            "complete": function(error) {
                if (error !== null) {
                    // Reached the timeout but not all fonts were loaded
                    console.log(error.message);
                    console.log(error.notLoadedFonts);
                } else {
                    // All fonts were loaded
                    console.log("all fonts were loaded");
                }
				
				// In All case
				GameCore.init();
            }
        }, 3000).loadFonts();
});