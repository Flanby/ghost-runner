var rootDir = "";

var scripts = document.getElementsByTagName("script"), arg = null;
rootDir = (arg = scripts[scripts.length - 1].src.split('?'))[0].split('/').slice(0, -2).join('/') + "/",
args = {};

if (arg.length > 1) {
    var rawargs = arg[1].split("&")

    for (var i = 0; rawargs.length > i; i++) {
        if (rawargs[i].length == 0)
            continue;

        var tmp = rawargs[i].split("=");
        args[tmp[0]] = true;

        if (tmp.length > 1)
            args[tmp[0]] = tmp[1];
    }
}
    

var list = [
    "Js/Libs/jquery.min.js",
    "Js/Libs/victor.min.js",
    "Js/Libs/pixi.min.js",
    "Js/Libs/FontLoader.js",
    "Js/Utils.js", 
    "Js/GameCore/Input.js", 
    "Js/Objects/Core/Bar.js", 
    "Js/Objects/Core/BaseObject.js", 
    "Js/Objects/Core/ObjectDisplay.js", 
    "Js/Objects/Core/MovingObject.js", 
    "Js/Objects/Player.js", 
    "Js/Objects/AI.js", 
    "Js/Objects/MapItems/Block.js", 
    "Js/Objects/MapItems/Exit.js", 
    "Js/GameCore/AnimSprite.js", 
    "Js/GameCore/Camera.js", 
    "Js/GameCore/Menu/Core/Clickable.js", 
    "Js/GameCore/Menu/Core/MenuItem.js", 
    "Js/GameCore/Menu/Core/Picto.js", 
    "Js/GameCore/Menu/Core/Button.js",
    "Js/GameCore/Menu/Core/Menu.js", 
    "Js/GameCore/Menu/Lobby.js", 
    "Js/GameCore/Menu/Winner.js", 
    "Js/GameCore/Scene/Game.js",
    "Js/GameCore/Scene/Main.js",
    "Js/GameCore/GameCore.js", 
    "Js/GameCore/GamePad.js", 
    "Js/Spells/Spell.js", 
    "Js/Spells/Dash.js", 
    "Js/Spells/Inflate.js",
    "Js/Spells/Blink.js",
    "Js/GameCore/Map/MapPattern.js",
    "Js/GameCore/Map/MapGenerator.js",
    "Js/GameCore/Background/Background.js",
    "Js/GameCore/Background/MenuBg.js",
    "Js/script.js",
];

if (typeof args.additionalscript != "undefined")
    list = list.concat(args.additionalscript.split(","));

var offset = 0;

function loadNextScript(dev = false) {
    var tag = document.createElement("script");
    tag.src = rootDir+list[offset++]+(dev ? "?"+new Date().getTime() : "");
    if (list.length > offset)
        tag.onload = loadNextScript.bind(loadNextScript, dev);
    document.getElementsByTagName("head")[0].appendChild(tag);
}

window.onload = function(){

    var style = document.createElement('style');
    style.type = 'text/css';

    style.innerHTML = `
@font-face {
	font-family: MotionControl-Bold;
	src: url(${rootDir}Assets/Fonts/MotionControl-Bold.otf)
}
`;
    (document.head || document.getElementsByTagName('head')[0]).appendChild(style);

    loadNextScript(arg.length > 1);
}