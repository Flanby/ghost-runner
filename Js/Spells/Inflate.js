
class Inflate extends Spell {
    constructor(owner) {
        super(owner, 100, 35, 88, 0xFFFF00, Bar.UpToDown);
        this.playerLifeStateRequired = false;
        //this.html.children[0].style.backgroundColor = "yellow";
        this.animationMod = null;
    }

    doTheSpell(target) {
        this.target = target;
        if (!this.activate())
            return;

        this.animationMod = this.target.anim;
        this.animationMod.scale.x *= 2;
        this.animationMod.scale.y *= 2;
        this.target.height *= 2;
		this.target.width *= 2;
    }

    endEffect() {
        this.animationMod.scale.x /= 2;
        this.animationMod.scale.y /= 2;
        this.target.height /= 2;
		this.target.width /= 2;
        super.endEffect();
    }
}
