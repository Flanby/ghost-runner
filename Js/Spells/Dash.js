
class Dash extends Spell {
    constructor(owner) {
        super(owner, 15, 2, 87, 0x00FF00, Bar.UpToDown);
        this.effect = 0;
        this.playerLifeStateRequired = true;
        //this.html.children[0].style.backgroundColor = "green";
    }

    doTheSpell(target) {
        this.target = target;
        if (!this.activate())
            return;

        this.effect = (this.target.isFaceRight ? 1 : -1) * this.target.maxSpeed * 2;
        this.continuousEffect();
    }

    continuousEffect() {
        this.target.movement.x = this.effect;
    }
}