
class Spell {
    constructor(owner, cooldown, duration, key, color = 0x00FF00, direction = Bar.UpToDown) {

        this.bar = new Bar(3, 100, color, direction).addBorder(1, 0x000000);
        this.bar.stage.visible = false;
        owner.metaDisplay.addChild(this.bar.stage);
        this.offset = this.bar.stage.width;

        this.cooldown = cooldown;
        this.cnt = 0;
        this.key = key;
        this.duration = duration;
        this.playerLifeStateRequired = false;
        this.target = null;
    }

    activate() {
        if (this.cnt > 0)
            return false;
        this.bar.stage.visible = false;
        this.cnt = this.cooldown + this.duration;
        this.bar.stage.position.set(-this.offset, 0);
        this.target.spellActive = true;
        return true;
    }

    getKey() {
        return this.key;
    }

    setKey(key) {
        this.key = key;
        return this;
    }

    update(el) {
        if (this.cnt > 0) {
            if (this.cnt >= this.cooldown) {
                this.cnt -= el;
                if (this.cnt < this.cooldown) {
                    this.target.spellActive = false;
                    this.endEffect();
                }
                else
                    this.continuousEffect();
            }
            else {
                this.cnt -= el;
                this.bar.resizeBacgroud(this.target.anim.height);
                this.bar.updateValue(this.cnt / this.cooldown);
            }
            
        }
        else if (this.cnt < 0) {
            this.bar.stage.visible = false;
            this.cnt = 0;
        }
    }

    doTheSpell(target) {
        this.target = target;
        if (!this.activate())
            return;
    }

    continuousEffect() {
        return ;
    }

    endEffect() {
        this.bar.stage.visible = true;
        this.bar.resize(this.target.anim.height);
        return;
    }
}