
class Blink extends Spell {
    constructor(owner) {
        super(owner, 15, 20, 67, 0xFF0000, Bar.UpToDown);
        this.offset *= 2;
        this.playerLifeStateRequired = true;
        //this.html.children[0].style.backgroundColor = "red";
    }

    doTheSpell(target) {
        this.target = target;
        if (!this.activate())
            return;

        this.target.spellActive = false;
        this.target.setOpacity(0.7);
        this.target.collideMask = this.target.collideMask & ~GameScene.mask.trigger.ghost;
    }

    endEffect() {
        this.target.setOpacity(1);
        this.target.collideMask = this.target.collideMask | GameScene.mask.trigger.ghost;
        super.endEffect();
    }
}