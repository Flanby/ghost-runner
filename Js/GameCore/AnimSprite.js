
class AnimData {
	constructor (file) {
		this.file = rootDir+file;
		this.anim = {};
	}

	addAnim(name, startFrame, endFrame, option = {}) {
		var defaultOption = {
			computeFromArea: 5250,
			size: null,
			scale: null,
			anchor: null,
			animSpeed: 0.12,
		};

		for (var key in option)
			defaultOption[key] = option[key];

		defaultOption.startFrame = startFrame;
		defaultOption.endFrame = endFrame;
		defaultOption.name = name;

		this.anim[name] = defaultOption;
		return this;
	}

	makeAnim(name, makenew = false) {
		if (!this.anim.hasOwnProperty(name))
			return false;

		var elem = this.anim[name], frames = [];

		if (!makenew && elem.hasOwnProperty("animSingl"))
			return elem.animSingl;

		for (var i = elem.startFrame; i <= elem.endFrame; i++)
			frames.push(PIXI.loader.resources[this.file].textures[i]);
		var anim = new PIXI.extras.AnimatedSprite(frames);

		if (elem.size) {
			anim.width = elem.size.width;
			anim.height = elem.size.height;
		}
		else if (elem.scale !== null) {
			anim.scale.set(elem.scale.x, elem.scale.y);
			elem.size = {width: anim.width, height: anim.height};
		}
		else {
			anim.width = frames[0].orig.width;
			anim.height = frames[0].orig.height;
			var ratio = anim.width / anim.height,
				aire = elem.computeFromArea;
			
			anim.width = Math.round((anim.height = Math.sqrt(aire/ratio)) * ratio);
			anim.height = Math.round(anim.height);
			elem.size = {width: anim.width, height: anim.height};
		}

		anim.animationSpeed = elem.animSpeed;
		elem.animSingl = anim;

		return anim;
	}
}