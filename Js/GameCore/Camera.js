
class Camera extends BaseObject {
	constructor(width, height) {
		super();

		this.setSize(width, height);
	}

	setSize(x, y) {
		super.setSize(x, y);

		this.offsetX = this.width * 0.4;
		this.offsetY = this.height * 0.2;

		if (typeof this.bg != "undefined" && this.bg !== null) {
			//this.bg.height = this.height;
			this.bg.width = this.width;

			this.skyRect.clear();
			// draw a shape
			this.skyRect.beginFill(0xB8DCFE, 1);
			this.skyRect.drawRect(0, -this.bg.height * 3, this.width, this.bg.height * 3);
			this.skyRect.endFill();
		}
	}

	frameFollowing(toFollow) {
		var x = 0, y = 0;

		if (toFollow.length == 0)
			return ;

		toFollow.forEach(function (elem) {
			x += elem.x + elem.width / 2;
			y += elem.y + elem.height / 2;
		});

		x /= toFollow.length;
		y /= toFollow.length;

		var minXcam = this.x + this.offsetX, maxXcam = this.x + this.width - this.offsetX;
		var minYcam = this.y + this.offsetY, maxYcam = this.y + this.height - this.offsetY;

		if (x < minXcam)
			this.x -= minXcam - x;
		else if (maxXcam < x)
			this.x += x - maxXcam;

		if (y < minYcam)
			this.y -= minYcam - y;
		else if (maxYcam < y)
			this.y += y - maxYcam;
	}

	keepOnCam(toKeep) {
		var minXcam = this.x, maxXcam = this.x + this.width;
		var minYcam = this.y, maxYcam = this.y + this.height;

		toKeep.forEach(function (elem) {
			var x = elem.x + elem.width / 2;
			var y = elem.y + elem.height / 2;

			if (x < minXcam)
				elem.x += minXcam - x;
			else if (maxXcam < x)
				elem.x -= x - maxXcam;

			if (y < minYcam)
				elem.y += minYcam - y;
			else if (maxYcam < y)
				elem.y -= y - maxYcam;		
		});
	}

	frameThemAll(mapstage, items, players) {
		var self = this, preCalculateX = (self.x + self.width / 2), preCalculateY = (self.y + self.height / 2);
		
		items.forEach(function (elem) {
			if (Math.abs((elem.x + elem.width / 2) - preCalculateX) * 2 <= (elem.width + self.width) &&
				Math.abs((elem.y + elem.height / 2) - preCalculateY) * 2 <= (elem.height + self.height)) {
				//elem.getHtml().style.display = "block";
				mapstage.addChild(elem.getAnim());
				elem.updateDisplay(self.x, self.y)
			}
			else
				//elem.getHtml().style.display = "none";
				mapstage.removeChild(elem.getAnim());
		});

		players.forEach(function (elem) {
			elem.updateDisplay(self.x, self.y)
			elem.metaDisplay.position.copy(elem.anim.position);
		});
	}

	shootThem(mapstage, items, players, selectToFallowCallback) {
		var toKeep = [], toFollow = [];

		players.forEach(function (player) {
			if (selectToFallowCallback(player))
				toFollow.push(player);
			else
				toKeep.push(player);
		});

		this.frameFollowing(toFollow);
		this.keepOnCam(toKeep);
		this.frameThemAll(mapstage, items, players);

		this.bglayer.position.y = -600 - this.y;
		this.bg.tilePosition.x = -this.x /2;
	}

	addBgLayer(bglayer) {
		this.bglayer = bglayer;
		return this;
	}

	addBg(texture) {
		this.skyRect = new PIXI.Graphics();

		// draw a shape
		this.skyRect.beginFill(0xB8DCFE, 1);
		this.skyRect.drawRect(0, -texture.height * 3, this.width, texture.height * 3);
		this.skyRect.endFill();

		this.bglayer.addChild(this.skyRect, this.bg = new PIXI.extras.TilingSprite(texture, this.width, texture.height));
		return this;
	}
}