

var controllers = {};
/*var rAF = window.requestAnimationFrame ||
          window.mozRequestAnimationFrame ||
          window.webkitRequestAnimationFrame;*/

function connecthandler(e) {
    addgamepad(e.gamepad);
}

function addgamepad(gamepad) {
    controllers[gamepad.index] = gamepad; 
    //rAF(updateStatus);
}

function disconnecthandler(e) {
    removegamepad(e.gamepad);
}

function removegamepad(gamepad) {
    //var d = document.getElementById("controller" + gamepad.index);
    //document.body.removeChild(d);
    delete controllers[gamepad.index];
}

function updateStatus() {
    scangamepads();
    for (j in controllers) {
        var controller = controllers[j];
        for (var i=0; i<controller.buttons.length; i++) {
            var val = controller.buttons[i];
            var pressed = val == 1.0;
            if (typeof(val) == "object")
                pressed = val.pressed;
            
            var btn = "gamepad-"+j+"-btn-"+i;
            if (pressed)
                Input.keydown(btn);
            else
                Input.keyup(btn);
        }

        for (var i=0; i<controller.axes.length; i++) {
            if (-0.1 < controller.axes[i] && controller.axes[i] < 0.1) {
                var btn = "gamepad-"+j+"-axe-"+i+"_";
                Input.keyup(btn+"-1");
                Input.keyup(btn+"1");
                continue;
            }
            Input.keydown("gamepad-"+j+"-axe-"+i+"_"+(controller.axes[i] / Math.abs(controller.axes[i])));
            
        }
    }
    //rAF(updateStatus);
}

function scangamepads() {
    var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
    for (var i = 0; i < gamepads.length; i++) {
        if (gamepads[i]) {
            if (!(gamepads[i].index in controllers)) {
                addgamepad(gamepads[i]);
            } else {
                controllers[gamepads[i].index] = gamepads[i];
            }
        }
    }
}

if ('GamepadEvent' in window) {
    window.addEventListener("gamepadconnected", connecthandler);
    window.addEventListener("gamepaddisconnected", disconnecthandler);
} else if ('WebKitGamepadEvent' in window) {
    window.addEventListener("webkitgamepadconnected", connecthandler);
    window.addEventListener("webkitgamepaddisconnected", disconnecthandler);
}

setInterval(updateStatus, 1000/60);