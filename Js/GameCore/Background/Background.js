
class Background {
    constructor(texture, w = null, h = null) {
        this.stage = new PIXI.Container();
        this.stage.addChild(this.bg = new PIXI.extras.TilingSprite(texture, w === null ? texture.width : w, texture.height));
        
        if (h !== null)
            this.bg.position.y = (h - texture.height) / 2;
    }

    setsize(w, h) {
        this.bg.width = w;
        this.bg.position.y = (h - this.bg.height) / 2;
    }

    getStage() {
        return this.stage;
    }

    runAnimation(delta) {
        return ;
    }
}