
class MenuBg extends Background {
    constructor(w, h) {
        super(PIXI.loader.resources[rootDir+"Assets/Map/BG/Graveyard.png"].texture, w, h);

        this.width = w;
        this.height = h;

        this.players = [];

        // Disable Monster on background
        // for (var i = 0; i < 6; i++) {
        //     var p = new Player(i, this.stage)
        //             .setPos(-100, -100)
        //             .setAlive(false)
        //             .setKeymap({
        //                 left: -1 - (i * 4), 
        //                 up: -2 - (i * 4), 
        //                 right: -3 - (i * 4), 
        //                 down: -4 - (i * 4)
        //             });
        //     p.nextChange = 0;
        //     this.players.push(p);
        // }
    }

    runAnimation(delta) {
        var dir = 0, preComputeX = (this.width) / 2, preComputeY = (this.height) / 2;

        delta /= 2;

        this.players.forEach(function(player) {
            if (Math.abs((player.x + player.width / 2) - preComputeX) * 2 <= (player.width + this.width) &&
                Math.abs((player.y + player.height / 2) - preComputeY) * 2 <= (player.height + this.height)) {
                if (player.nextChange <= 0) {

                    Input.keyup(player.keymap.down);
                    Input.keyup(player.keymap.up);
                    Input.keyup(player.keymap.right);
                    Input.keyup(player.keymap.left);

                    dir = Math.random() * 9 + 1;
                    player.nextChange = Math.random() * 2000 + 300;

                    if (dir < 4)
                        Input.keydown(player.keymap.up);
                    if (2 < dir && dir < 6)
                        Input.keydown(player.keymap.right)
                    if (4 < dir && dir < 8)
                        Input.keydown(player.keymap.down);
                    if (6 < dir || dir < 2)
                        Input.keydown(player.keymap.left);
                }
                player.nextChange -= delta;
            }
            else {
                // Re-center
                if (player.y < 500)
                    Input.keydown(player.keymap.down);
                else
                    Input.keyup(player.keymap.down);

                if (player.y > this.height - 500)
                    Input.keydown(player.keymap.up);
                else
                    Input.keyup(player.keymap.up);

                if (player.x < 500)
                    Input.keydown(player.keymap.right);
                else
                    Input.keyup(player.keymap.right);

                if (player.x > this.width - 500)
                    Input.keydown(player.keymap.left);
                else
                    Input.keyup(player.keymap.left);
                
                player.nextChange = 0; 
            }

            player.run(delta);
            player.updateDisplay(0, 0);
        }, this);
    }

    setsize(w, h) {
        super.setsize(w, h);
        this.bg.tilePosition.x = (w - 1100) / 2;
    }
}