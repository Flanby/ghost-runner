
var GameScene = {
	x: 0,
	y: 0,

	mask: {
		collide: {
			ghost:  0b00000001,
			player: 0b00000010
		},
		trigger: {
			ghost:  0b00010000,
			player: 0b00100000
		}
	},

	objects: [],
	players: [],
	others: [],

	gravity: 15,

	inUse: false,

	phatomSpeed: 10,
	phatomFrein: 4,

	playerSpeed: 15,
	playerFrein: 15,
	jumpPower: 75,

	playerMaxSpeed: 50,
	phatomMaxSpeed: 75,

	takeOverTime: 30,
	takeOverReductor: new Victor(4, 4),
    isloaded: false,

	sprites: {
		monstersTranslate: ["Eye_Monster", "Ghost_Monster", "Bomb_Monster", "Yellow_Monster", "Moskito" , "Red_Skull", "Bat", "Dark_Bat"],
		monsters: {
			Eye_Monster: new AnimData("Assets/Monsters/Eye_Monster.json")
				.addAnim("idle", 0, 7),
			Ghost_Monster: new AnimData("Assets/Monsters/Ghost_Monster.json")
				.addAnim("idle", 0, 7),
			Bomb_Monster: new AnimData("Assets/Monsters/Bomb_Monster.json")
				.addAnim("idle", 0, 7),
			Yellow_Monster: new AnimData("Assets/Monsters/Yellow_Monster.json")
				.addAnim("idle", 0, 7),
			Moskito: new AnimData("Assets/Monsters/Moskito.json")
				.addAnim("idle", 0, 7),
			Red_Skull: new AnimData("Assets/Monsters/Red_Skull.json")
				.addAnim("idle", 0, 7),
			Bat: new AnimData("Assets/Monsters/Bat.json")
				.addAnim("idle", 0, 7, {animSpeed:0.15}),
			Dark_Bat: new AnimData("Assets/Monsters/Dark_Bat.json")
				.addAnim("idle", 0, 7)
		},
		player: new AnimData("Assets/Rabbit/Anim.json")
			.addAnim("dash", 0, 2, {scale: {x: 0.115, y: 0.115}})
			.addAnim("idle", 3, 4, {animSpeed:0.05, scale: {x: 0.107, y: 0.107}})
			.addAnim("run", 5, 8)
	},

	camera: null,
	
	winner: null,
	winnerWindow: null,

	addItem: function(item) {
		this.objects.push(item);

		if (item instanceof Player) {
			this.players.push(item);
			this.stage.ui.addChild(item.metaDisplay);
		}
		else
			this.others.push(item);
	},

	setWinner: function(player) {
		this.winnerWindow.setWinner(this.winner = player);
		this.centerMenu(this.winnerWindow);
		this.stage.addChild(this.winnerWindow.stage);
	},

	run: function(elapsed) {
		if (this.inUse)
			return;
		this.inUse = true;

		if (this.winner == null) {
			this.objects.forEach(function(elem) {
				elem.run(elapsed);
			});

			this.checkCollide();
			this.display();
		}

		this.inUse = false;
        return this.stage;
	},

    centerMenu: function(win) {
		win.stage.position.set((GameCore.x - win.width) / 2, (GameCore.y - win.height) / 2 + 75);
    },

	display: function() {
		this.camera.shootThem(this.stage.map, this.others, this.players, function(player) {return player.isAlive});
	},

	checkCollide: function() {
		self = this;
		var colliders = [];
		var len = this.objects.length;

		for (var i = 0; i < len; i++) {
			var elem = this.objects[i];
			if (elem.isMoving()) {
				for (var j = i+1; j < len; j++) {
					var comp = this.objects[j];
					//console.log("Mask: "+elem.collideMask+"/"+comp.collideMask+" = "+(elem.collideMask & comp.collideMask));
					if ((elem.collideMask & comp.collideMask) &&
						Math.abs((elem.x + elem.width / 2) - (comp.x + comp.width / 2)) * 2 <= (elem.width + comp.width) &&
						Math.abs((elem.y + elem.height / 2) - (comp.y + comp.height / 2)) * 2 <= (elem.height + comp.height)) {

						//console.log("Mask: Ok");
						colliders.push([elem, comp]);
					}
				}
				elem.collideWithPrev = elem.collideWith;
				elem.collideWith = [];
			}
		}

		colliders.forEach(function(coll) {
			var item1 = coll[0], item2 = coll[1], 
				deltaX = item1.x - item2.x, deltaXinv = deltaX * -1,
				deltaY = item1.y - item2.y, deltaYinv = deltaY * -1;
			var collTypeX = 0, collTypeY = 0; // -1 = Left / Up, 1 = Right / Down, 0 = center

			// Calcul Collision X
			if (deltaX <= 0 && deltaXinv <= item1.width) {
				if (deltaXinv + item2.width <= item1.width){
					collTypeX = 0;
					deltaX = item2.width;
				}
				else {
					collTypeX = -1;
					deltaX += item1.width;
				}
			}
			else if (deltaX > 0 && deltaX <= item2.width) {
				if (deltaX + item1.width <= item2.width){
					collTypeX = 0;
					deltaX = item1.width;
				}
				else {
					collTypeX = 1;
					deltaX = (deltaX - item2.width) * -1;
				}
			}

			// Calcul Collision Y
			if (deltaY <= 0 && deltaYinv <= item1.height) {
				if (deltaYinv + item2.height <= item1.height) {
					collTypeY = 0;
					deltaY = item2.height;
				}
				else {
					collTypeY = -1;
					deltaY += item1.height;
				}
			}
			else if (deltaY > 0 && deltaY <= item2.height) {
				if (deltaY + item1.height <= item2.height) {
					collTypeY = 0;
					deltaY = item1.height;
				}
				else {
					collTypeY = 1;
					deltaY = (deltaY - item2.height) * -1;
				}
			}

			if (0b11110000 & item2.collideMask & item1.collideMask) {
				item1.collideWith.push(item2);
				item2.collideWith.push(item1);
				
				if (!item1.collideWithPrev.contain(item2)) {
					item1.onCollisionStart(item2, collTypeX, collTypeY, deltaX, deltaY);
					item2.onCollisionStart(item1, collTypeX, collTypeY, deltaX, deltaY);
				}
				item1.onCollision(item2, collTypeX, collTypeY, deltaX, deltaY);
				item2.onCollision(item1, collTypeX, collTypeY, deltaX, deltaY);
			}

			// Correction
			if (deltaX == 0 || deltaY == 0 || (0b00001111 & item2.collideMask & item1.collideMask) == 0 || 
				(item2 instanceof Player && !item1.isAlive && !item2.isAlive)){
				return;
			}
			if ((deltaY < deltaX || collTypeX == 0) && collTypeY != 0) {
				item1.y += deltaY * collTypeY;
				item1.movement = new Victor(item1.movement.x, 0);
			}
			else if ((deltaY > deltaX || collTypeY == 0) && collTypeX != 0) {
				item1.x += deltaX * collTypeX;	
				item1.movement = new Victor(0, item1.movement.y);
			}
		});

		// Collision Stop
		this.objects.forEach(function(elem) {
			if (elem.isMoving()) {
				elem.collideWithPrev.forEach(function(item){
					if (!elem.collideWith.contain(item)) {
						item.collideWithPrev.remove(elem);

						item.onCollisionStop(elem);
						elem.onCollisionStop(item);
					}
				});
			}
		});
	},

	setFullsreenMode: function(mode, width, height) {
		if (mode)
			this.camera.y = -400;
		
		this.x = width;
		this.y = height;

		this.camera.setSize(width, height);
		this.isFullScreen = mode;
	},

	clear: function() {
		this.objects = [];
		this.players = [];
		this.others = [];

		this.winner = null;
		this.stage.removeChild(this.winnerWindow.stage);

		this.stage.map.removeChildren()
		this.stage.player.removeChildren()
		this.stage.ui.removeChildren()
	},

	init: function(renderer, width, height) {
		this.inUse = true;

		GameCore.renderer.transparent = false;
		GameCore.renderer._backgroundColorRgba[3] = 1;
		GameCore.renderer.backgroundColor = 0xe5a029;

		this.x = width;
		this.y = height;

        if (this.isloaded) {
            this.inUse = false;
			this.clear();
            if (typeof this.onGameReady != "undefined")
                this.onGameReady();
        }
        else {
			this.winnerWindow = new Winner("Winner");
            this.camera = new Camera(this.x, this.y);
            this.stage = new PIXI.Container();

            this.stage.addChild(this.stage.bg = new PIXI.Container(), this.stage.map = new PIXI.Container(), this.stage.player = new PIXI.Container(), this.stage.ui = new PIXI.Container());

			this.camera.addBgLayer(this.stage.bg).addBg(PIXI.loader.resources[rootDir+"Assets/Map/BG/Desert.png"].texture);

			this.isloaded = true;
			this.inUse = false;
			if (typeof this.onGameReady != "undefined")
				this.onGameReady();
        }

	}
};
