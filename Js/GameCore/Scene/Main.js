
var MainScene = {
	x: 0,
	y: 0,

	inUse: false,
    isloaded: false,

    title: "",
    items: [],
    offset: 0,

    keymap: Object.assign(Input.keymaps.Move.Arrows, Input.keymaps.Enter),

	run: function(elapsed) {
		if (this.inUse)
			return;
		this.inUse = true;

        if (Input.isKeyPressEdge(this.keymap.up))
            this.items[this.offset].moveUp();
        else if (Input.isKeyPressEdge(this.keymap.down))
            this.items[this.offset].moveDown();
        else if (Input.isKeyPressEdge(this.keymap.enter))
            this.items[this.offset].activate();

        this.bg.runAnimation(elapsed);

		this.inUse = false;
        return this.stage;
	},

    create_menus: function() {

        this.items.push(new Menu("pause", "Ghost Run", [
            {title: "Play", selected: true, callback: function() { MainScene.changeMenu(2); }},
            {title: "Option", callback: function() { MainScene.changeMenu(1); }},
            {title: "How To", active: false, callback: function() {}},
            {title: "Credit", active: false, callback: function() {}},
            {title: "Exit", active: true, callback: function() { GameCore.setFullsreenMode(false); }}
        ]));

        this.items.push(new Menu("win", "Option", [
            {title: "Video", active: true, callback: function() { MainScene.changeMenu(3); }},
            {title: "Sound", active: false, callback: function() {}},
            {title: "KeyBinding", active: false, callback: function() {}},
            {title: "Return", selected: true, callback: function() { MainScene.changeMenu(0); }}
        ]));

        this.items.push(new Lobby("Game Lobby"));

        this.items.push(new Menu("win", "Video", [
            {title: "FullScreen", callback: function() {
                var elem = $('html')[0],
                    requestFullScreen = elem.requestFullScreen || elem.webkitRequestFullScreen || elem.mozRequestFullScreen || elem.msRequestFullScreen;

                console.log(requestFullScreen.call(elem));
                setTimeout(function(){ GameCore.setFullsreenMode(true); }, 100);
            }},
            {title: "ViewPort", callback: function() { GameCore.setFullsreenMode(true); }},
            {title: "Normal", callback: function() { GameCore.setFullsreenMode(false); }},
            {title: "Return", selected: true, callback: function() { MainScene.changeMenu(1); }}
        ]))

        //this.items.push(new Menu("big", "Lobby", [
        //    {title: "Play", active: true, callback: function() { MainScene.changeMenu(0); GameCore.changeScene(1); }},
        //    {title: "Return", selected: true, callback: function() { MainScene.changeMenu(0); }}
        //]));

        
        this.bg = new MenuBg(this.x, this.y);

        this.stage.addChild(this.bg.getStage(), this.items[0].stage);
        this.centerMenu(this.items[0].stage);
    },

    changeMenu: function(newMenu) {
        this.stage.removeChild(this.items[this.offset].stage);
        this.stage.addChild(this.items[this.offset = newMenu].stage);
        this.centerMenu(this.items[this.offset].stage);
    },

    centerMenu: function(win) {
        win.position.set((this.x - win.width) / 2, (this.y - win.height) / 2);
    },

	setFullsreenMode: function(mode, width, height) {
		this.x = width;
		this.y = height;

        this.bg.setsize(this.x, this.y);
        this.centerMenu(this.items[this.offset].stage);
	},

    init: function(renderer, width, height) {
		this.inUse = true;

		GameCore.renderer.transparent = true;
		GameCore.renderer._backgroundColorRgba[3] = 0;


		this.x = width;
		this.y = height;

        if (this.isloaded) {
            this.inUse = false;
        }
        else {
            this.stage = new PIXI.Container();

            // Load For Menu
            var toLoad = [rootDir+"Assets/UI/Picto-0.json", rootDir+"Assets/UI/Picto-1.json", rootDir+"Assets/UI/Windows.json"];

            // Load For Game
            toLoad.push(rootDir+"Assets/Map/Map.json");
            toLoad.push(rootDir+"Assets/Map/Exit.png");

            for (monster in GameScene.sprites.monsters)
                toLoad.push(GameScene.sprites.monsters[monster].file);

            toLoad.push(GameScene.sprites.player.file);

            // Load BG
            toLoad.push(rootDir+"Assets/Map/BG/Desert.png");
            toLoad.push(rootDir+"Assets/Map/BG/Graveyard.png");
            toLoad.push(rootDir+"Assets/Map/BG/Winter.png");

            // Loading Screen
            var bar = new Bar(20, this.x / 2, 0x00FF00, Bar.LeftToRight).addBorder(3, 0x000000),
                msg = new PIXI.Text("Ghost Runner\nLoading ...",{fontFamily : 'MotionControl-Bold', fontSize: 40, fill : 0xFFFFFF, align : 'center', strokeThickness: 2, stroke: 0x000000}),
                filename = new PIXI.Text("",{fontFamily : 'MotionControl-Bold', fontSize: 20, fill : 0xFFFFFF, align : 'center', strokeThickness: 2, stroke: 0x000000}),
                loadingStage = new PIXI.Container();
            
            loadingStage.addChild(bar.stage, msg, filename);

            // Place all elem
            bar.updateValue(0);
            bar.stage.position.set(this.x / 4, this.y / 2);
            msg.position.set(this.x / 2 - msg.width / 2, this.y / 2 - (msg.height + 10));
            filename.position.set(this.x / 4, this.y / 2 + 56);

            renderer.render(loadingStage);

            PIXI.loader.add(toLoad)
            .on("progress", function loadProgressHandler(loader, resource) {
                bar.updateValue(loader.progress / 100 / 2);
                filename.setText(resource.url);
                renderer.render(loadingStage);
                //console.log(resource.url);
            })
            .load(function setup() {
                // For Game
                for (var key in GameScene.sprites.monsters) {
                    for (var j in GameScene.sprites.monsters[key].anim) {
                        var anim = GameScene.sprites.monsters[key].makeAnim(j);
                        anim.play();;
                    }
                }

                for (var j in GameScene.sprites.player.anim) {
                    var anim = GameScene.sprites.player.makeAnim(j);
                    anim.play();
                }
                
                MainScene.isloaded = true;
                MainScene.create_menus();
                MainScene.inUse = false;
            });
        }
	}
};