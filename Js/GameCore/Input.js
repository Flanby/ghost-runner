
var Input = {

    keymaps: {
        Move: {
            Arrows: {left: 37, up: 38, right: 39, down: 40},
            ZQSD: {left: 81, up: 90, right: 68, down: 83},
            Pad: {left: 100, up: 104, right: 102, down: 101},
            GamePad0: {left: "gamepad-0-axe-0_-1", up: "gamepad-0-axe-1_-1", right: "gamepad-0-axe-0_1", down: "gamepad-0-axe-1_1"},
            GamePad1: {left: "gamepad-1-axe-0_-1", up: "gamepad-1-axe-1_-1", right: "gamepad-1-axe-0_1", down: "gamepad-1-axe-1_1"}
        },
        Enter: {enter: 13}
    },

	keypress: [],
    keydownKey: [],

    keydown: function(key) {
        if (key == 27)
            //GameCore.setFullsreenMode(false);
            GameCore.changeScene(0);
        if (this.isKeyPress(key))
            return;
        this.keydownKey.push(key);
        if (!this.isKeyPress(key)) {
            this.keypress.push(key);
        }
    },

    keyup: function(key) {
        this.keypress.remove(key);
        this.keydownKey.remove(key);
    },

    isKeyPress: function(key) {
        return this.keypress.indexOf(key) != -1;
    },

    isKeyPressEdge: function(key) {
        var res = this.isKeyPress(key);
        if (res)
            this.keypress.remove(key);
        return res;
    }
}

$(document).keydown(function(e) {
    if (GameCore.isPaused)
        return true;

    if ([17, 82, 16, 116, 123].indexOf(e.which) == -1)
        e.preventDefault();
    Input.keydown(e.which);
});
$(document).keyup(function(e) {
    if (GameCore.isPaused)
        return true;

    Input.keyup(e.which);
    e.preventDefault();
});