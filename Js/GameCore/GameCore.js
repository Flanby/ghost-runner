
var GameCore = {
	x: 0,
	y: 0,
	isFullScreen: false,
	isPaused: false,

	lastTime: performance.now(),
	inUse: false,

	HTMLcontainer: document.getElementById("game"),

	scene: [MainScene, GameScene],
	actual: 0,

	setPause: function(state) {
		if (this.isPaused === state)
			return ;

		if (!state)
			this.lastTime = performance.now();

		this.isPaused = state;
	},

	changeScene: function(newstate) {
		this.actual = newstate;
		this.scene[this.actual].init(this.renderer, this.x, this.y);
		this.scene[this.actual].setFullsreenMode(this.isFullScreen, this.x, this.y);
	},

	run: function() {
		if (this.inUse || this.isPaused)
			return;
		this.inUse = true;

		var elapsed = performance.now() - this.lastTime;
		this.lastTime = performance.now()
		if (elapsed > 100)
			elapsed = 17;

		if (!this.scene[this.actual].inUse)
			this.renderer.render(this.scene[this.actual].run(elapsed));
		
		this.inUse = false;
	},

	setFullsreenMode: function(mode) {
		if (this.isFullScreen != mode) {
			if (mode)
				this.HTMLcontainer.classList.add("fullscreen");
			else
				this.HTMLcontainer.classList.remove("fullscreen");
		}
		
		this.x = this.HTMLcontainer.offsetWidth;
		this.y = this.HTMLcontainer.offsetHeight;

		this.renderer.resize(this.x, this.y);
		this.scene[this.actual].setFullsreenMode(this.isFullScreen = mode, this.x, this.y);
	},

	init: function() {
		this.inUse = true;

		this.x = this.HTMLcontainer.offsetWidth;
		this.y = this.HTMLcontainer.offsetHeight;

		this.renderer = PIXI.autoDetectRenderer(this.x, this.y, {antialias: false, transparent: true, resolution: 1});


		this.HTMLcontainer.appendChild(this.renderer.view);
		this.scene[this.actual].init(this.renderer, this.x, this.y);
		this.inUse = false;
		
		this.lastTime = performance.now();
		rAF(function callback(){ GameCore.run(); rAF(callback); });
	}
};
