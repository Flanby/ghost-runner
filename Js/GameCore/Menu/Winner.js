
class Winner extends Menu {
    constructor(title) {
        super("info", title, []);

        this.stage.children[1].y += 0;
        this.stage.children[2].y += 0; 

        this.width = this.stage.width;
        this.height = this.stage.height;

        var crown = new PIXI.Sprite(PIXI.loader.resources[rootDir+"Assets/UI/Windows.json"].textures["Windows_top_crown.png"]),
            light = new PIXI.Sprite(PIXI.loader.resources[rootDir+"Assets/UI/Windows.json"].textures["Windows_top_suncast.png"]);

        crown.position.set((this.stage.children[0].width - crown.width) / 2, this.stage.children[1].y - crown.height + 20);
        light.position.set((this.stage.children[0].width - light.width) / 2, this.stage.children[2].y + this.stage.children[2].height - light.height);

        this.stage.addChildAt(crown, 0);
        this.stage.addChildAt(light, 0);

        this.items.push(new Button(0, "Replay", MenuItem.STYLE.ACTIVE, () => GameScene.init(), true).setParentMenu(this));
        this.items.push(new Button(1, "Return", MenuItem.STYLE.DEFAULT, () => GameCore.changeScene(0), true).setParentMenu(this));

        this.items[0].getGraphics().scale.set(0.75, 0.75);
        this.items[1].getGraphics().scale.set(0.75, 0.75);

        var off = 50,
            voff = 75, 
            wd2 = (this.width - off * 2) / 2, 
            buttw = this.items[0].getGraphics().width, 
            h = (this.height - voff * 2) / 4 * 3 + voff;

        this.items[0].getGraphics().position.set(off + (wd2 - buttw) / 2, h);
        this.items[1].getGraphics().position.set(off + (wd2 - buttw) / 2 + wd2, h);

        this.stage.addChild(this.items[0].getGraphics());
        this.stage.addChild(this.items[1].getGraphics());

        this.text = new PIXI.Text("N/a", {fontFamily : 'MotionControl-Bold', fontSize: 50, fill : 0xE4DFDA, align : 'center', strokeThickness: 2, stroke: 0x000000});

        this.text.position.set((this.width - this.text.width) / 2, h / 2);
        this.stage.addChild(this.text);
    }

    setWinner(player) {
        //new PIXI.Text(player.name, {fontFamily : 'MotionControl-Bold', fontSize: 50, fill : player.title.style.fill, align : 'center', strokeThickness: 2, stroke: 0x000000});
        //new PIXI.Text(player.name, {fontFamily : 'MotionControl-Bold', fontSize: 50, fill : 0xE4DFDA, align : 'center', strokeThickness: 2, stroke: 0x000000});  
        this.text.setText(player.name);
        this.text.style.fill = player.title.style.fill;
        this.text.position.x = (this.width - this.text.width) / 2;
    }
}