
class Clickable {
    constructor(callback, enable = true) {
        this.callback = callback;
        this.enable = enable;
        this.parent = null;
    }

    click() {
        if (!this.isEnable())
            return ;

        if (typeof this.callback === 'function')
            this.callback();
        else
            this.callback[0][this.callback[1]](this);
    }

    setCallback(func) {
        this.callback = callback;
    }

    setEnable(bool) {
        this.enable = bool;
    }

    isEnable() {
        return this.enable;
    }

    hasParent() {
        return this.parent !== null;
    }

    setParent(p) {
        this.parent = p;
    }

    getParent() {
        return this.parent;
    }
}