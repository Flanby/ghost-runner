
class Button extends MenuItem {
    constructor(id, title, status, callback, enable) {
        super(id, status, callback, enable);

        this.title = title;
        this.craftGrapics(status);
    }
    
    craftGrapics(type) {
        if (this.isGraphicsExist(type))
            return ;

        var button = new PIXI.Container(),
            bg = new PIXI.Sprite(PIXI.loader.resources[rootDir+"Assets/UI/Picto-0.json"].textures[type.style.bg]),
            butttitle = new PIXI.Text(this.title, {fontFamily : 'MotionControl-Bold', fontSize: 55, fill : [type.style.top, type.style.bot], fillGradientType: PIXI.TEXT_GRADIENT.LINEAR_VERTICAL, align : 'center', strokeThickness: 1, stroke: type.style.stroke});

        bg.scale.set(0.75, 0.5);

        button.addChild(bg, butttitle);
        this.center_item(butttitle, bg.width, bg.height);
        
        super.craftGrapics(button, type);
    }

    // To Do Make utils function
    center_horizontaly(item, width) {
        item.x = (width - item.width) / 2;
        return item;
    }

    center_verticaly(item, height) {
        item.y = (height - item.height) / 2;
        return item;
    }

    center_item(item, width, height) {
        return this.center_horizontaly(this.center_verticaly(item, height), width);
    }

}