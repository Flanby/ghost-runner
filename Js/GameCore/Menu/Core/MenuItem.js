
class MenuItem extends Clickable {
    constructor(id, style, c, e) {
        super(c, e);

        this.id = id;
        this.style = style;
        this.g = {};
    }

    craftGrapics(graphics, type) {
        if (this.parentMenu instanceof Menu)
            this.setClickable(graphics);
        graphics.style = type;
        this.g[type.name] = graphics;
    }

    getGraphics() {
        return this.g[this.style.name];
    }

    isGraphicsExist(style) {
        return typeof this.g[status.name] !== "undefined";
    }

    changeStyle(style) {
        if (this.isGraphicsExist(style))
            this.style = style
        else {
            var pos = this.getGraphics().position,
                scale = this.getGraphics().scale;
        
            this.craftGrapics(style);
            this.style = style
            this.getGraphics().position.copy(pos);
            this.getGraphics().scale.copy(scale);
        }

        return this.getGraphics();
    }

    setParentMenu(parent) {
        this.parentMenu = parent;

        Object.keys(this.g).forEach(function(index) { 
            this.setClickable(this.g[index]);
        }, this);

        return this;
    }

    setClickable(button) {
        if (button.style != MenuItem.STYLE.LOCKED && !button.buttonMode) {
            var self = this;

            button.interactive = true;
            button.buttonMode = true;
            button.on("mouseover", function() { 
                if (self.parentMenu instanceof Menu) 
                    self.parentMenu.changeSelection(self.id); 
            });
            button.on("pointerdown", function() { 
                if (self.parentMenu instanceof Menu) 
                    self.parentMenu.activate(); 
            });
        }
    }

}


MenuItem.STYLE = {
    DEFAULT: {id: 0, name: "default", fileSufix: "", style: {bg: "Empty_button.png", top: 0x5c9a00, bot: 0x73c100, stroke: 0x4b7507} },
    ACTIVE: {id: 1, name: "active", fileSufix: "_active", style: {bg: "Empty_button_active.png", top: 0x8eb900, bot: 0xabe000, stroke: 0x749400} },
    CLICK: {id: 2, name: "click", fileSufix: "_click", style: {bg: "Empty_button_click.png", top: 0x317100, bot: 0x449d00, stroke: 0x254c05} },
    LOCKED: {id: 3, name: "locked", fileSufix: "_lock", style: {bg: "Empty_button_lock.png", top: 0x859696, bot: 0xa5afaf, stroke: 0x6c7777} }
}