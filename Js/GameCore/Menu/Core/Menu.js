
class Menu {
    constructor(type, title, items) {
        var types = type.split("|");
        this.stage = this.craft_window(types[0], (types.length >= 2 ? types[1] : types[0]), title);

        this.items = items;
        this.cursor = 0;

        items.forEach(function(elem, num) {
            var type = elem.active === false ? MenuItem.STYLE.LOCKED : elem.selected === true ? MenuItem.STYLE.ACTIVE : MenuItem.STYLE.DEFAULT;
            this.items[num] = new Button(num, elem.title, type, elem.callback, !(elem.active === false)).setParentMenu(this);
            this.placeItem(num);

            if (elem.selected === true)
                this.cursor = num;

            this.stage.addChild(this.items[num].getGraphics());
        }, this);
    }

    changeItemStatus(item, newstatus) {
        if (item.status == newstatus.name)
            return ;
        var stage = item.hasParent() ? item.getParent().stage : this.stage;

        stage.removeChild(item.getGraphics());
        stage.addChild(item.changeStyle(newstatus));
    }

    enableButton(item, status) {
        this.changeItemStatus(item, status ? MenuItem.STYLE.DEFAULT : MenuItem.STYLE.LOCKED);
        item.setEnable(status);
    }

    changeSelection(SelectedMenu) {
        if (SelectedMenu == this.cursor)
            return;

        this.changeItemStatus(this.items[this.cursor], MenuItem.STYLE.DEFAULT);
        this.changeItemStatus(this.items[this.cursor = SelectedMenu], MenuItem.STYLE.ACTIVE);
    }

    move(direction) {
        var newCursor = this.cursor;

        do {
            newCursor += direction;
            if (newCursor < 0)
                newCursor = this.items.length - 1;
            if (newCursor >= this.items.length)
                newCursor = 0;

            // No other button active - rageQuit
            if (this.cursor == newCursor)
                return ;
        } while (!this.items[newCursor].enable);

        this.changeSelection(newCursor);
    }

    moveUp() {
        this.move(-1);
    }

    moveDown() {
        this.move(+1);
    }

    activate() {
       this.items[this.cursor].click();
    }

    craft_window(type, headType, title = null) {
        var win = new PIXI.Container(),
            bg = new PIXI.Sprite(PIXI.loader.resources[rootDir+"Assets/UI/Windows.json"].textures["Windows_back_"+type+".png"]),
            head = title == null ? null : new PIXI.Sprite(PIXI.loader.resources[rootDir+"Assets/UI/Windows.json"].textures["Windows_top_"+headType+".png"]);
        win.addChild(bg);

        if (head != null) {
            win.addChild(head);
            head.position.set((bg.width - head.width) / 2, -head.height / 4);
            var wintitle = new PIXI.Text(title, {fontFamily : 'MotionControl-Bold', fontSize: 55, fill : [0x9ebfb9, 0xb8d1cc], fillGradientType: PIXI.TEXT_GRADIENT.LINEAR_VERTICAL, align : 'center', strokeThickness: 1, stroke: 0x7f9995});
            win.addChild(wintitle);
            wintitle.position.set(head.x + (head.width - wintitle.width) / 2, head.y + (head.height - wintitle.height) / 3);
        }

        return win;
    }

    placeItem(num) {
        var button = this.items[num].getGraphics();
        button.position.set(
            (this.stage.children[0].width - button.children[0].width) / 2, 
            ((this.stage.children[0].height - this.items.length * button.children[0].height - this.stage.children[1].height * 0.75 * 2) / (this.items.length + 1)) * (num + 1) + num * button.children[0].height + this.stage.children[1].height * 0.75
        );
    }

    // To Do Make utils function
    center_horizontaly(item, width) {
        item.x = (width - item.width) / 2;
        return item;
    }

    center_verticaly(item, height) {
        item.y = (height - item.height) / 2;
        return item;
    }

    center_item(item, width, height) {
        return this.center_horizontaly(this.center_verticaly(item, height), width);
    }
}