
class Picto extends MenuItem {
    constructor(id, picto, status, callback, enable) {
        super(id, status, callback, enable);

        this.picto = picto;
        this.craftGrapics(status);
    }

    craftGrapics(style) {
        if (this.isGraphicsExist(style))
            return true;
        
        var realfile = this.picto.split("."),
            picto = realfile[0] + style.fileSufix + "." + realfile[1];

        if (PIXI.loader.resources[rootDir+"Assets/UI/Picto-0.json"].textures.hasOwnProperty(picto))
            picto = new PIXI.Sprite(PIXI.loader.resources[rootDir+"Assets/UI/Picto-0.json"].textures[picto]);
        else
            picto = new PIXI.Sprite(PIXI.loader.resources[rootDir+"Assets/UI/Picto-1.json"].textures[picto]);
        picto.scale.set(0.5, 0.5);

        super.craftGrapics(picto, style);
    }
}