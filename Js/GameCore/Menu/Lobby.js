
class Lobby extends Menu {
    constructor(title) {
        super("big", title, []);

        var margin = 100,
            size = (this.stage.children[0].width - 2 * margin) / 4,
            cnt = 0;

        this.playerHolder = [];
        for (var i = 0; i < 4; i++) {
            this.playerHolder.push({
                stage: new PIXI.Container(), 
                selection: 0, 
                button_left:  new Picto(cnt++, "Picto_Left.png", MenuItem.STYLE.DEFAULT, [this, "change_device_select_left"], true),
                button_right: new Picto(cnt++, "Picto_Right.png", MenuItem.STYLE.DEFAULT, [this, "change_device_select_right"], true),
                text_display: new PIXI.Text(Lobby.DEVICELIST[0].name, {fontFamily: 'MotionControl-Bold', fontSize: 45, fill: 0xae842e, align: 'center'}),
                width: size
            });

            this.center_horizontaly(this.playerHolder[i].text_display, size);
            this.center_horizontaly(this.playerHolder[i].button_left.getGraphics(), size / 2)
            this.center_horizontaly(this.playerHolder[i].button_right.getGraphics(), size / 2).x += size / 2;
            
            this.playerHolder[i].button_left.getGraphics().y = this.playerHolder[i].button_right.getGraphics().y = this.playerHolder[i].text_display.height + 10;

            this.playerHolder[i].button_left.setParent(this.playerHolder[i]); 
            this.playerHolder[i].button_right.setParent(this.playerHolder[i]);

            this.playerHolder[i].stage.addChild(this.playerHolder[i].text_display, this.playerHolder[i].button_left.getGraphics(), this.playerHolder[i].button_right.getGraphics());
            this.playerHolder[i].stage.position.set(margin + i * size, 300);

            this.items.push(this.playerHolder[i].button_left);
            this.items.push(this.playerHolder[i].button_right);

            this.stage.addChild(this.playerHolder[i].stage);
        }

        this.items.push(new Button(cnt++, "Play", MenuItem.STYLE.LOCKED, [this, "Launch_Game"], false));
        this.items.push(new Button(cnt++, "Return", MenuItem.STYLE.ACTIVE, function() { MainScene.changeMenu(0); }, true));

        var ret = this.items.length - 1, play = ret - 1;

        this.center_horizontaly(this.items[play].getGraphics(), 2 * size).x += margin;
        this.items[play].getGraphics().y = 500;
        this.items[ret].getGraphics().x = this.items[play].getGraphics().x + 2 * size;
        this.items[ret].getGraphics().y = 500;

        this.stage.addChild(this.items[play].getGraphics());
        this.stage.addChild(this.items[ret].getGraphics());

        this.cursor = ret;

        this.items.forEach(function(elem) {
            elem.setParentMenu(this)
        }, this);
    }

    Launch_Game() {
        delete GameScene.onGameReady;
        var self = this;

        GameScene.onGameReady = function() {
            var cnt = 0, offset = 300;
            self.playerHolder.forEach(function(elem) { if (elem.selection != 0) { cnt++; } });

            cnt = Math.floor(Math.random() * cnt);

            self.playerHolder.forEach(function(elem, index) {
                if (elem.selection == 0)
                    return ;

                if (Lobby.DEVICELIST[elem.selection].name == "AI")
                    GameScene.addItem(new AI(index)
                             .setName("AI "+(index + 1), Lobby.DEVICELIST[elem.selection].color)
                             .setPos(offset + 175 * (index + 1), 200)
                             .setAlive(cnt-- == 0)
                             .setKeymap(Lobby.DEVICELIST[elem.selection].keymap));
                else
                    GameScene.addItem(new Player(index)
                             .setName("Player "+(index + 1), Lobby.DEVICELIST[elem.selection].color)
                             .setPos(offset + 175 * (index + 1), 200)
                             .setAlive(cnt-- == 0)
                             .setKeymap(Lobby.DEVICELIST[elem.selection].keymap));
            });
            
            mapGenerator.genMap(25).forEach(function(element) {
                GameScene.addItem(element);
            });
        }

        MainScene.changeMenu(0);
        GameCore.changeScene(1);
    }

    change_device_select_left(elem) {
        this.change_device_select(elem, -1);
    }

    change_device_select_right(elem) {
        this.change_device_select(elem, +1);
    }

    change_device_select(elem, dir) {
        var p = elem.getParent();

        p.selection += dir;

        if (p.selection < 0)
            p.selection = Lobby.DEVICELIST.length - 1;
        if (Lobby.DEVICELIST.length <= p.selection)
            p.selection = 0;

        p.text_display.setText(Lobby.DEVICELIST[p.selection].name);
        this.center_horizontaly(p.text_display, elem.parent.width);

        var cnt = 0;
        for (var i = 0; i < this.playerHolder.length; i++) {
            if (this.playerHolder[i].selection != 0) {
                cnt ++;
            }
        }

        this.enableButton(this.items[this.items.length - 2], cnt >= 2);
    }
}

Lobby.DEVICELIST = [
    { name:"Empty", keymap: null },
    { name:"Arrows", color: 0x66ff33, keymap: Input.keymaps.Move.Arrows },
    { name:"ZQSD", color: 0xffff00, keymap: Input.keymaps.Move.ZQSD },
    { name:"Pad", color: 0xff99ff, keymap: Input.keymaps.Move.Pad },
    { name:"GamePad 1", color: 0x33cccc, keymap: Input.keymaps.Move.GamePad0 },
    { name:"GamePad 2", color: 0xff3300, keymap: Input.keymaps.Move.GamePad1 },
    { name: "AI", color: 0xff0000, keymap: {left: "AI", up: "AI", right: "AI", down: "AI"} }
];