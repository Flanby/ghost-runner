patterns = [
    {
        id: 0,
        "name": "Start",
        "row": 9,
        "col": 16,
        "blocs": [
            {
                "x": 0,
                "y": 3,
                "w": 4,
                "h": 6,
                "tiles": [
                    "Map/Sci-fi/Tiles/Acid (1).png",
                    "Map/Sci-fi/Tiles/Acid (1).png",
                    "Map/Sci-fi/Tiles/Acid (1).png",
                    "Map/Sci-fi/Tiles/Tile (6).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Tile (6).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Tile (6).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Tile (5).png"
                ]
            },
            {
                "x": 3,
                "y": 0,
                "w": 1,
                "h": 3,
                "tiles": [
                    "Map/Sci-fi/Tiles/Tile (3).png",
                    "Map/Sci-fi/Tiles/Tile (6).png",
                    "Map/Sci-fi/Tiles/Tile (6).png"
                ]
            },
            {
                "x": 4,
                "y": 6,
                "w": 6,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/11.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 10,
                "y": 6,
                "w": 6,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            }
        ],
        "rowOffset": 0,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[2]
    },
    {
        id: 1,
        "name": "End",
        "row": 9,
        "col": 12,
        "blocs": [
            {
                "x": 0,
                "y": 6,
                "w": 6,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 6,
                "y": 6,
                "w": 3,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/7.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 9,
                "y": 3,
                "w": 3,
                "h": 6,
                "tiles": [
                    "Map/Sci-fi/Tiles/Tile (4).png",
                    "Map/Sci-fi/Tiles/Acid (1).png",
                    "Map/Sci-fi/Tiles/Acid (1).png",
                    "Map/Sci-fi/Tiles/Tile (4).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Tile (4).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Acid (2).png",
                    "Map/Sci-fi/Tiles/Acid (2).png"
                ]
            },
            {
                "x": 9,
                "y": 1,
                "w": 1,
                "h": 2,
                "tiles": [
                    "Map/Sci-fi/Tiles/Tile (1).png",
                    "Map/Sci-fi/Tiles/Tile (4).png"
                ]
            }
        ],
        "specials": [
            {"type": "Exit", "x": 8, "y": 5}
        ],
        "rowOffset": 0,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[2, 7, 8]
    },
    {
        id: 2,
        "name": "Flat",
        "row": 6,
        "col": 11,
        "blocs": [
            {
                "x": 0,
                "y": 3,
                "w": 6,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 2,
                "y": 2,
                "w": 3,
                "h": 1,
                "tiles": [
                    "Map/Winter/Tiles/Crate.png",
                    "Map/Winter/Tiles/Crate.png",
                    "Map/Winter/Tiles/Crate.png"
                ]
            },
            {
                "x": 6,
                "y": 3,
                "w": 5,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            }
        ],
        "rowOffset": 3,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[2, 3, 8]
    },
    {
        id: 3,
        "name": "Falling",
        "row": 8,
        "col": 7,
        "blocs": [
            {
                "x": 0,
                "y": 6,
                "w": 6,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/10.png",
                    "Map/Forest/Tiles/11.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 0,
                "y": 1,
                "w": 1,
                "h": 5,
                "tiles": [
                    "Map/Forest/Tiles/3.png",
                    "Map/Forest/Tiles/6.png",
                    "Map/Forest/Tiles/6.png",
                    "Map/Forest/Tiles/6.png",
                    "Map/Forest/Tiles/6.png"
                ]
            },
            {
                "x": 4,
                "y": 1,
                "w": 3,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 6,
                "y": 6,
                "w": 1,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/5.png"
                ]
            }
        ],
        "rowOffset": 5,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[4, 5, 6]
    },
    {
        id: 4,
        "name": "MiddleGap",
        "row": 8,
        "col": 8,
        "blocs": [
            {
                "x": 0,
                "y": 6,
                "w": 6,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 2,
                "y": 0,
                "w": 4,
                "h": 0.7265625,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 3,
                "y": 4,
                "w": 2,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png"
                ]
            },
            {
                "x": 4,
                "y": 3,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png"
                ]
            },
            {
                "x": 6,
                "y": 6,
                "w": 2,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            }
        ],
        "rowOffset": 5,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[4, 5, 6, 7]
    },
    {
        id: 5,
        "name": "MiddleGap2",
        "row": 9,
        "col": 11,
        "blocs": [
            {
                "x": 0,
                "y": 7,
                "w": 5,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/10.png",
                    "Map/Forest/Tiles/11.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/7.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 0,
                "y": 6,
                "w": 2,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/3.png"
                ]
            },
            {
                "x": 4,
                "y": 2,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 5,
                "y": 6,
                "w": 6,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/10.png",
                    "Map/Forest/Tiles/11.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/8.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 5,
                "y": 5,
                "w": 4,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/1.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/3.png"
                ]
            }
        ],
        "rowOffset": 5,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[4, 5, 6, 7]
    },
    {
        id: 6,
        "name": "MiddleGap3",
        "row": 9,
        "col": 11,
        "blocs": [
            {
                "x": 0,
                "y": 7,
                "w": 6,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/10.png",
                    "Map/Forest/Tiles/11.png",
                    "Map/Forest/Tiles/7.png",
                    "Map/Forest/Tiles/8.png",
                    "Map/Forest/Tiles/10.png",
                    "Map/Forest/Tiles/11.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 0,
                "y": 6,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/3.png"
                ]
            },
            {
                "x": 1,
                "y": 1,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 3,
                "y": 5,
                "w": 2,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/1.png",
                    "Map/Forest/Tiles/3.png",
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/6.png"
                ]
            },
            {
                "x": 6,
                "y": 7,
                "w": 5,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/7.png",
                    "Map/Forest/Tiles/8.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 7,
                "y": 2,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 9,
                "y": 6,
                "w": 2,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/1.png",
                    "Map/Forest/Tiles/2.png"
                ]
            }
        ],
        "rowOffset": 5,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[4, 5, 6, 7]
    },
    {
        id: 7,
        "name": "Rising",
        "row": 9,
        "col": 11,
        "blocs": [
            {
                "x": 0,
                "y": 7,
                "w": 5,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/10.png",
                    "Map/Forest/Tiles/11.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/7.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 0,
                "y": 6,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/3.png"
                ]
            },
            {
                "x": 1,
                "y": 3,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 5,
                "y": 6,
                "w": 6,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/10.png",
                    "Map/Forest/Tiles/11.png",
                    "Map/Forest/Tiles/7.png",
                    "Map/Forest/Tiles/8.png",
                    "Map/Forest/Tiles/8.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 5,
                "y": 5,
                "w": 3,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/1.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/3.png"
                ]
            },
            {
                "x": 5,
                "y": 0,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 9,
                "y": 3,
                "w": 2,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/4.png"
                ]
            },
            {
                "x": 10,
                "y": 1,
                "w": 1,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/1.png",
                    "Map/Forest/Tiles/4.png"
                ]
            }
        ],
        "rowOffset": 5,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[2, 8]
    },
    {
        id: 8,
        "name": "rand",
        "row": 11,
        "col": 12,
        "blocs": [
            {
                "x": 0,
                "y": 8,
                "w": 6,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/7.png",
                    "Map/Forest/Tiles/8.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 2,
                "y": 2,
                "w": 4,
                "h": 0.7265625,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 3,
                "y": 6,
                "w": 3,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/1.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/3.png",
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/10.png"
                ]
            },
            {
                "x": 4,
                "y": 1,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png"
                ]
            },
            {
                "x": 6,
                "y": 8,
                "w": 6,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/10.png",
                    "Map/Forest/Tiles/11.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 6,
                "y": 7,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/3.png"
                ]
            },
            {
                "x": 8,
                "y": 4,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 10,
                "y": 7,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png"
                ]
            },
            {
                "x": 11,
                "y": 6,
                "w": 1,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png"
                ]
            }
        ],
        "rowOffset": -2,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[2, 8]
    }
];