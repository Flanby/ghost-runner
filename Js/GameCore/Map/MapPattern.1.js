//var patternMask = {plainBlock: }
var patterns = [
	{
       "id":1,
        "name":"Start",
        "pattern":[
            [3,0,0,0,0],
            [6,0,0,0,0],
            [6,0,0,0,0],
            [6,0,0,0,0],
            [10,11,2,2,2]
        ],
        "ChanceOfSpawn":100,
        "Difficulty":0.0,
        "nextTo":["3","4","7","8","9"]
    },
	{
       "id":2,
        "name":"End",
        "pattern":[
            [0,0,0,0,1],
            [0,0,0,0,4],
            [0,0,0,0,4],
            [0,0,0,0,4],
            [2,2,2,7,8]
        ],
        "ChanceOfSpawn":0,
        "Difficulty":0.0,
        "nextTo":[""]
    },
	{
       "id":3,
        "name":"Empty",
        "pattern":[
            [0,0,0,0,0],
            [0,0,0,0,0],
            [0,0,0,0,0],
            [0,0,0,0,0],
            [2,2,2,2,2]
        ],
        "ChanceOfSpawn":100,
        "Difficulty":2.0,
        "nextTo":["4","7","9","11","13"]
    },
	{
       "id":4,
        "name":"Platform",
        "pattern":[
            [0,0,0,0,0],
            [0,0,0,0,0],
            [0,0,0,0,0],
            [0,1,2,3,0],
            [7,8,5,10,11]
        ],
        "ChanceOfSpawn":100,
        "Difficulty":3.0,
        "nextTo":["7","8","9","3","11","13"]
    },
	{
       "id":5,
        "name":"TheWall",
        "pattern":[
            [0,0,0,0,2],
            [0,0,0,0,2],
            [0,0,0,0,2],
            [0,0,0,0,2],
            [0,0,0,0,2]
        ],
        "ChanceOfSpawn":100,
        "Difficulty":3.0,
        "nextTo":[""]
    },
	{
       "id":6,
        "name":"The secondWall",
        "pattern":[
            [2,0,0,0,0],
            [2,0,0,0,0],
            [2,0,0,0,0],
            [2,0,0,0,0],
            [2,0,0,0,0]
        ],
        "ChanceOfSpawn":100,
        "Difficulty":3.0,
        "nextTo":[""]
    },
	{
       "id":7,
        "name":"Obstacle",
        "pattern":[
            [1,3,0,0,0],
            [12,16,0,0,0],
            [0,0,0,0,19],
            [0,0,0,1,20],
            [2,2,2,8,10]
        ],
        "ChanceOfSpawn":50,
        "Difficulty":5.0,
        "nextTo":["3","4","8","13"]
    },
	{
       "id":8,
        "name":"Random",
        "pattern":[
            [0,14,0,0,0],
            [0,0,0,13,15],
            [13,15,0,0,0],
            [0,0,0,19,0],
            [2,2,2,5,2]
        ],
        "ChanceOfSpawn":100,
        "Difficulty":7.0,
        "nextTo":["3","4","7","8","11"]
    },
	{
       "id":9,
        "name":"StartPyramid",
        "pattern":[
            [0,0,0,0,1],
            [0,0,0,1,8],
            [0,0,1,8,5],
            [0,1,8,5,5],
            [7,8,5,5,5]
        ],
        "ChanceOfSpawn":50,
        "Difficulty":5.0,
        "nextTo":["10"]
    },
	{
       "id":10,
        "name":"EndPyramid",
        "pattern":[
            [3,0,0,0,0],
            [10,3,0,0,0],
            [5,10,3,0,0],
            [5,5,10,3,0],
            [5,5,5,10,11]
        ],
        "ChanceOfSpawn":70,
        "Difficulty":5.0,
        "nextTo":["3","4","7","8","13"]
    },
	{
       "id":11,
        "name":"StartReversePyramid",
        "pattern":[
            [13,2,2,2,2],
            [0,12,5,5,5],
            [0,0,12,5,5],
            [0,0,0,4,5],
            [2,2,2,8,5]
        ],
        "ChanceOfSpawn":50,
        "Difficulty":5.0,
        "nextTo":["12"]
    },
	{
       "id":12,
        "name":"EndReversePyramid",
        "pattern":[
            [2,2,2,2,0],
            [2,2,2,0,0],
            [2,2,0,0,0],
            [2,0,0,0,0],
            [2,2,2,2,2]
        ],
        "ChanceOfSpawn":250,
        "Difficulty":9.0,
        "nextTo":["3","4","7","8","9"]
    },
	{
       "id":13,
        "name":"UpperPlatform",
        "pattern":[
            [0,0,0,0,0],
            [2,2,2,2,2],
            [0,0,0,0,0],
            [0,0,0,0,0],
            [2,2,2,2,2]
        ],
        "ChanceOfSpawn":100,
        "Difficulty":4.0,
        "nextTo":["3","4","7","8","9","11"]
    }
];

patterns = [
    {
        "id":0,
        "name": "Start",
        "row": 8,
        "col": 10,
        "blocs": [
            {
                "x": 0,
                "y": 0,
                "w": 1,
                "h": 8,
                "tiles": [
                    "Map/Forest/Tiles/3.png",
                    "Map/Forest/Tiles/6.png",
                    "Map/Forest/Tiles/6.png",
                    "Map/Forest/Tiles/6.png",
                    "Map/Forest/Tiles/6.png",
                    "Map/Forest/Tiles/6.png",
                    "Map/Forest/Tiles/6.png",
                    "Map/Forest/Tiles/10.png"
                ]
            },
            {
                "x": 1,
                "y": 6,
                "w": 1,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/11.png"
                ]
            },
            {
                "x": 2,
                "y": 5,
                "w": 2,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png"
                ]
            },
            {
                "x": 4,
                "y": 7,
                "w": 6,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png"
                ]
            },
            {
                "x": 8,
                "y": 6,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png"
                ]
            }
        ],
        "rowOffset": -3,
        "ChanceOfSpawn": 100,
        "Difficulty": 5.0,
        "nextTo":[8]
    },
    {
        "id": 1,
        "name": "End",
        "row": 8,
        "col": 10,
        "blocs": [
            {
                "x": 0,
                "y": 7,
                "w": 6,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png"
                ]
            },
            {
                "x": 1,
                "y": 4,
                "w": 3,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 4,
                "y": 6,
                "w": 2,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png"
                ]
            },
            {
                "x": 6,
                "y": 7,
                "w": 3,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/7.png"
                ]
            },
            {
                "x": 9,
                "y": 0,
                "w": 1,
                "h": 8,
                "tiles": [
                    "Map/Forest/Tiles/1.png",
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/4.png",
                    "Map/Forest/Tiles/8.png"
                ]
            }
        ],
        "rowOffset": -3,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[2, 3]
    },
    {
        id: 2,
        "name": "Lambdas",
        "row": 5,
        "col": 15,
        "blocs": [
            {
                "x": 0,
                "y": 4,
                "w": 6,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png"
                ]
            },
            {
                "x": 1,
                "y": 1,
                "w": 3,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 6,
                "y": 4,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/2.png"
                ]
            },
            {
                "x": 7,
                "y": 2,
                "w": 1,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/2.png"
                ]
            },
            {
                "x": 8,
                "y": 0,
                "w": 3,
                "h": 5,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png"
                ]
            },
            {
                "x": 11,
                "y": 4,
                "w": 4,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png"
                ]
            },
            {
                "x": 11,
                "y": 2,
                "w": 1,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png"
                ]
            }
        ],
        "rowOffset": 0,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[2, 3, 4]
    },
    {
        "id": 3,
        "name": "test",
        "row": 5,
        "col": 5,
        "blocs": [
            {
                "x": 0,
                "y": 2,
                "w": 5,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Forest/Tiles/18.png",
                    "Map/Forest/Tiles/18.png",
                    "Map/Forest/Tiles/18.png",
                    "Map/Forest/Tiles/18.png",
                    "Map/Forest/Tiles/18.png"
                ]
            },
            {
                "x": 2,
                "y": 1,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png"
                ]
            },
            {
                "x": 3,
                "y": 0,
                "w": 1,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/Crate.png",
                    "Map/Forest/Tiles/Crate.png"
                ]
            }
        ],
        "rowOffset": 1,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[2, 3]
    },
    {
        "id": 4,
        "name": "Clif Start",
        "row": 11,
        "col": 15,
        "blocs": [
            {
                "x": 0,
                "y": 6,
                "w": 4,
                "h": 5,
                "tiles": [
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/10.png",
                    "Map/Forest/Tiles/11.png",
                    "Map/Forest/Tiles/3.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/6.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/10.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 0,
                "y": 4,
                "w": 2,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/3.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/6.png"
                ]
            },
            {
                "x": 4,
                "y": 8,
                "w": 6,
                "h": 3,
                "tiles": [
                    "Map/Forest/Tiles/11.png",
                    "Map/Forest/Tiles/3.png",
                    "Map/Sci-fi/Tiles/Tile (12).png",
                    "Map/Sci-fi/Tiles/Tile (13).png",
                    "Map/Sci-fi/Tiles/Tile (13).png",
                    "Map/Sci-fi/Tiles/Tile (14).png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/10.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Winter/Tiles/18.png",
                    "Map/Winter/Tiles/18.png",
                    "Map/Winter/Tiles/18.png",
                    "Map/Winter/Tiles/18.png"
                ]
            },
            {
                "x": 10,
                "y": 8,
                "w": 5,
                "h": 3,
                "tiles": [
                    "Map/Sci-fi/Tiles/Tile (1).png",
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (4).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Tile (4).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Tile (5).png"
                ]
            }
        ],
        "rowOffset": 0,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[5]
    },
    {
        "id": 5,
        "name": "Down Cliff - Sf to desert",
        "row": 8,
        "col": 15,
        "blocs": [
            {
                "x": 0,
                "y": 5,
                "w": 2,
                "h": 3,
                "tiles": [
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (3).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Tile (6).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Sci-fi/Tiles/Tile (5).png"
                ]
            },
            {
                "x": 2,
                "y": 7,
                "w": 6,
                "h": 1,
                "tiles": [
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (2).png"
                ]
            },
            {
                "x": 3,
                "y": 3,
                "w": 4,
                "h": 1,
                "tiles": [
                    "Map/Sci-fi/Tiles/Tile (12).png",
                    "Map/Sci-fi/Tiles/Tile (13).png",
                    "Map/Sci-fi/Tiles/Tile (13).png",
                    "Map/Sci-fi/Tiles/Tile (14).png"
                ]
            },
            {
                "x": 7,
                "y": 6,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Desert/Tiles/1.png"
                ]
            },
            {
                "x": 8,
                "y": 5,
                "w": 4,
                "h": 3,
                "tiles": [
                    "Map/Desert/Tiles/1.png",
                    "Map/Desert/Tiles/8.png",
                    "Map/Desert/Tiles/10.png",
                    "Map/Desert/Tiles/3.png",
                    "Map/Desert/Tiles/8.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/10.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png"
                ]
            },
            {
                "x": 9,
                "y": 4,
                "w": 2,
                "h": 1,
                "tiles": [
                    "Map/Desert/Tiles/1.png",
                    "Map/Desert/Tiles/3.png"
                ]
            },
            {
                "x": 12,
                "y": 6,
                "w": 1,
                "h": 2,
                "tiles": [
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/5.png"
                ]
            },
            {
                "x": 13,
                "y": 4,
                "w": 2,
                "h": 4,
                "tiles": [
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png"
                ]
            },
            {
                "x": 13,
                "y": 3,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Desert/Tiles/StoneBlock.png"
                ]
            }
        ],
        "rowOffset": 4,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[6]
    },
    {
        "id": 6,
        "name": "Down Cliff - desert",
        "row": 8,
        "col": 15,
        "blocs": [
            {
                "x": 0,
                "y": 6,
                "w": 6,
                "h": 2,
                "tiles": [
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png"
                ]
            },
            {
                "x": 1,
                "y": 2,
                "w": 3,
                "h": 1,
                "tiles": [
                    "Map/Desert/Tiles/14.png",
                    "Map/Desert/Tiles/15.png",
                    "Map/Desert/Tiles/16.png"
                ]
            },
            {
                "x": 5,
                "y": 4,
                "w": 1,
                "h": 2,
                "tiles": [
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png"
                ]
            },
            {
                "x": 6,
                "y": 2,
                "w": 2,
                "h": 6,
                "tiles": [
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png"
                ]
            },
            {
                "x": 6,
                "y": 0,
                "w": 1,
                "h": 2,
                "tiles": [
                    "Map/Desert/Tiles/StoneBlock.png",
                    "Map/Desert/Tiles/StoneBlock.png"
                ]
            },
            {
                "x": 8,
                "y": 6,
                "w": 6,
                "h": 2,
                "tiles": [
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png"
                ]
            },
            {
                "x": 11,
                "y": 5,
                "w": 3,
                "h": 1,
                "tiles": [
                    "Map/Desert/Tiles/Crate.png",
                    "Map/Desert/Tiles/Crate.png",
                    "Map/Desert/Tiles/Crate.png"
                ]
            },
            {
                "x": 14,
                "y": 6,
                "w": 1,
                "h": 2,
                "tiles": [
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/5.png"
                ]
            }
        ],
        "rowOffset": 4,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[7, 6]
    },
    {
        "id": 7,
        "name": "Up Cliff",
        "row": 8,
        "col": 15,
        "blocs": [
            {
                "x": 0,
                "y": 6,
                "w": 6,
                "h": 2,
                "tiles": [
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Desert/Tiles/5.png"
                ]
            },
            {
                "x": 6,
                "y": 5,
                "w": 5,
                "h": 3,
                "tiles": [
                    "Map/Sci-fi/Tiles/Tile (1).png",
                    "Map/Sci-fi/Tiles/Tile (13).png",
                    "Map/Sci-fi/Tiles/Tile (13).png",
                    "Map/Sci-fi/Tiles/Tile (13).png",
                    "Map/Sci-fi/Tiles/Tile (3).png",
                    "Map/Sci-fi/Tiles/Tile (5).png",
                    "Map/Winter/Tiles/17.png",
                    "Map/Winter/Tiles/17.png",
                    "Map/Winter/Tiles/17.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Desert/Tiles/5.png",
                    "Map/Forest/Tiles/18.png",
                    "Map/Forest/Tiles/18.png",
                    "Map/Forest/Tiles/18.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 10,
                "y": 4,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/1.png"
                ]
            },
            {
                "x": 11,
                "y": 3,
                "w": 4,
                "h": 5,
                "tiles": [
                    "Map/Forest/Tiles/1.png",
                    "Map/Forest/Tiles/8.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/8.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 12,
                "y": 2,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/1.png"
                ]
            },
            {
                "x": 13,
                "y": 1,
                "w": 2,
                "h": 2,
                "tiles": [
                    "Map/Forest/Tiles/1.png",
                    "Map/Forest/Tiles/8.png",
                    "Map/Forest/Tiles/8.png",
                    "Map/Forest/Tiles/5.png"
                ]
            },
            {
                "x": 14,
                "y": 0,
                "w": 1,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/1.png"
                ]
            }
        ],
        "rowOffset": 4,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[2]
    },
    {
        "id": 8,
        "name": "Test Plateform",
        "row": 6,
        "col": 30,
        "blocs": [
            {
                "x": 0,
                "y": 5,
                "w": 6,
                "h": 1,
                "tiles": [
                    "Map/Winter/Tiles/2.png",
                    "Map/Winter/Tiles/2.png",
                    "Map/Winter/Tiles/2.png",
                    "Map/Winter/Tiles/2.png",
                    "Map/Winter/Tiles/2.png",
                    "Map/Sci-fi/Tiles/Tile (2).png"
                ]
            },
            {
                "x": 1,
                "y": 3,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Winter/Tiles/14.png",
                    "Map/Winter/Tiles/15.png",
                    "Map/Winter/Tiles/16.png"
                ]
            },
            {
                "x": 6,
                "y": 5,
                "w": 6,
                "h": 1,
                "tiles": [
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Sci-fi/Tiles/Tile (2).png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png"
                ]
            },
            {
                "x": 6,
                "y": 1,
                "w": 3,
                "h": 0.50390625,
                "tiles": [
                    "Map/Sci-fi/Tiles/Tile (12).png",
                    "Map/Sci-fi/Tiles/Tile (13).png",
                    "Map/Sci-fi/Tiles/Tile (14).png"
                ]
            },
            {
                "x": 11,
                "y": 3,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Desert/Tiles/14.png",
                    "Map/Desert/Tiles/15.png",
                    "Map/Desert/Tiles/16.png"
                ]
            },
            {
                "x": 12,
                "y": 5,
                "w": 6,
                "h": 1,
                "tiles": [
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Desert/Tiles/2.png",
                    "Map/Graveyard/Tiles/Tile (2).png",
                    "Map/Graveyard/Tiles/Tile (2).png",
                    "Map/Graveyard/Tiles/Tile (2).png"
                ]
            },
            {
                "x": 16,
                "y": 1,
                "w": 3,
                "h": 0.7421875,
                "tiles": [
                    "Map/Graveyard/Tiles/Tile (14).png",
                    "Map/Graveyard/Tiles/Tile (15).png",
                    "Map/Graveyard/Tiles/Tile (16).png"
                ]
            },
            {
                "x": 18,
                "y": 5,
                "w": 6,
                "h": 1,
                "tiles": [
                    "Map/Graveyard/Tiles/Tile (2).png",
                    "Map/Graveyard/Tiles/Tile (2).png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png",
                    "Map/Forest/Tiles/2.png"
                ]
            },
            {
                "x": 21,
                "y": 3,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 24,
                "y": 5,
                "w": 6,
                "h": 1,
                "tiles": [
                    "Map/Forest/Tiles/2.png",
                    "Map/Winter/Tiles/Crate.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Forest/Tiles/17.png",
                    "Map/Winter/Tiles/Crate.png"
                ]
            },
            {
                "x": 26,
                "y": 1,
                "w": 3,
                "h": 0.7421875,
                "tiles": [
                    "Map/Graveyard/Tiles/Tile (14).png",
                    "Map/Graveyard/Tiles/Tile (15).png",
                    "Map/Graveyard/Tiles/Tile (16).png"
                ]
            },
            {
                "x": 26,
                "y": 0,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Forest/Tiles/13.png",
                    "Map/Forest/Tiles/14.png",
                    "Map/Forest/Tiles/15.png"
                ]
            },
            {
                "x": 26,
                "y": 4,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Winter/Tiles/14.png",
                    "Map/Winter/Tiles/15.png",
                    "Map/Winter/Tiles/16.png"
                ]
            },
            {
                "x": 26,
                "y": 2,
                "w": 3,
                "h": 0.7265625,
                "tiles": [
                    "Map/Desert/Tiles/14.png",
                    "Map/Desert/Tiles/15.png",
                    "Map/Desert/Tiles/16.png"
                ]
            },
            {
                "x": 26,
                "y": 3,
                "w": 3,
                "h": 0.50390625,
                "tiles": [
                    "Map/Sci-fi/Tiles/Tile (12).png",
                    "Map/Sci-fi/Tiles/Tile (13).png",
                    "Map/Sci-fi/Tiles/Tile (14).png"
                ]
            }
        ],
        "rowOffset": -1,
        "ChanceOfSpawn": 100,
        "Difficulty": 5,
        "nextTo":[2, 3]
    }
];