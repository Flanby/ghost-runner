
var SpecialBlocks = {"Exit": Exit };

var mapGenerator = {
    patterns: patterns,

    init: function() {
/*        this.patterns.forEach(function(element) {
            element.blocs = [];
            element.pattern.forEach(function(row, rowindex) {
                var cnt = 0, last = [];
                for (var i = 0; i < row.length; i++) {
                    if (row[i] != 0) {
                        cnt++;
                        last.push(row[i]);
                    }
                    else if (cnt > 0) {
                        this.blocs.push(new Block(last, cnt, 1, i - cnt, rowindex));
                        cnt = 0;
                        last = [];
                    }
                }

                if (cnt > 0)
                    this.blocs.push(new Block(last, cnt, 1, row.length - cnt, rowindex));
                cnt = 0;
            }, element);
        }, this);*/
    },

    
    addPattern: function(list, pattern, len) {
        pattern.blocs.forEach(function(elem) {
            list.push(new Block(elem.tiles, elem.w, elem.h, elem.x + len, elem.y + pattern.rowOffset));
        });
        if (typeof pattern.specials != "undefined")
            pattern.specials.forEach(function(elem) {
                if (elem.type == null || typeof SpecialBlocks[elem.type] == "undefined") {
                    console.warn("Special Block '" + elem.type + "' found in '" + pattern.name + "' do not exist");
                    return ;
                }
                list.push(new SpecialBlocks[elem.type](elem.x + len, elem.y + pattern.rowOffset));
            });
    },

    genMap: function(size) {
        var list = [], prev = "", rand = 0, cnt = 1, len = 0;
        size -= 1;

        this.addPattern(list, this.patterns[0], 0);
        prev = this.patterns[0].nextTo;
        len += this.patterns[0].col;

        while (cnt < size || !this.patterns[1].nextTo.contain(rand)) {
            rand = parseInt(prev[Math.floor(Math.random() * prev.length)]);

            if (this.patterns[rand].nextTo.length != 0) {
                //console.log(this.patterns[rand].name);
                this.addPattern(list, this.patterns[rand], len);
                len += this.patterns[rand].col;
                prev = this.patterns[rand].nextTo;
                cnt++;
            }
        }

        this.addPattern(list, this.patterns[1], len);

        return list;
    }
}
