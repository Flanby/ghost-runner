
class Player extends MovingObject {
	constructor(id, mainStage = null) {
		super();
		this.id = id;
		this.jump = 1;

		this.takeOver = 0;
		this.takeOverdir = 0;

		this.isFaceRight = true;
		this.opacity = 1;

		this.keymap = Input.keymaps.Move.Arrows;

		this.contactToOtherSide = 0;

		this.metaDisplay = new PIXI.Container();
		this.spells = [new Dash(this), new Blink(this), new Inflate(this)];
		this.spellActive = false;

		this.parentStage = mainStage === null ? GameScene.stage.player : mainStage;

		this.anim = null;
		this.setAlive(true);

		this.takeOverBar = new Bar(3, 100, 0x00FF00, Bar.LeftToRight).addBorder(1, 0x000000);
		this.takeOverBar.stage.visible = false;
		this.metaDisplay.addChild(this.takeOverBar.stage);
	}

	setKeymap(keymap) {
		this.keymap = keymap;
		return this;
	}

	setOpacity(opa) {
		this.opacity = opa;
		if (this.anim != null)
			this.anim.alpha = this.opacity;
	}

	resetTakeOver() {
		this.takeOver = 0;
		this.takeOverdir = 0;
		//this.takeOverBar.style.display = "none";
		this.takeOverBar.stage.visible = false;
	}

	setAlive(isIt) {
		if (this.isAlive == isIt)
			return this;
		this.isAlive = isIt;

		// Rest all Spells
		this.spells.forEach(function(spell) {
			if (spell.cnt > 0) {
				if (spell.cnt > spell.cooldown)
					spell.endEffect();
				spell.cnt = -1;
				spell.update(-1);
			}
		});
		this.spellActive = false;

		if (this.isAlive) {
			this.controlSpeed = false;
			this.setMaxSpeed(GameScene.playerMaxSpeed);
			this.setCollideMask(GameScene.mask.collide.player | GameScene.mask.trigger.player | GameScene.mask.trigger.ghost);
			this.changeAnim(GameScene.sprites.player.makeAnim("idle"));
		}
		else {
			this.controlSpeed = true;
			this.setMaxSpeed(GameScene.phatomMaxSpeed);
			this.setCollideMask(GameScene.mask.collide.ghost | GameScene.mask.trigger.ghost);
			this.changeAnim(GameScene.sprites.monsters[GameScene.sprites.monstersTranslate[this.id]].makeAnim("idle"));
		}

		return this;
	}

	changeAnim(anim) {
		if (this.anim != anim) {
			this.parentStage.removeChild(this.anim);
			this.anim = anim;
			this.anim.alpha = this.opacity;
			this.parentStage.addChild(this.anim);
			this.setSize(this.anim.width, this.anim.height);

			if (this.isFaceRight && this.anim.anchor.x == 1) {
				this.anim.anchor.x = 0;
				this.anim.scale.x = Math.abs(this.anim.scale.x);
			}
			else if (!this.isFaceRight && this.anim.anchor.x == 0) {
				this.anim.anchor.x = 1;
				this.anim.scale.x *= -1;
			}
		}
	}

	run(elapsed) {
		var el = elapsed / 100.0;

		if (this.takeOverdir != 0) {
			this.takeOver += el * this.takeOverdir;
			this.takeOverBar.resizeBacgroud(this.anim.width);
			this.takeOverBar.updateValue(this.takeOver / GameScene.takeOverTime);
			if (this.takeOver < 0)
				this.resetTakeOver();
		}


		var freinCoef = GameScene.phatomFrein, speedCoef = GameScene.phatomSpeed;
		if (this.isAlive) {
			freinCoef = GameScene.playerFrein;
			speedCoef = GameScene.playerSpeed;
		}

		// Get input
		var direction = new Victor(0, 0);
		if (this.hasOwnProperty("isIA") && this.isIA)
			direction = this.AIdirection;
		else {
			if (Input.isKeyPress(this.keymap.up)){
				if (this.isAlive){
					if (this.jump > 0) {
						this.movement = new Victor(this.movement.x, -GameScene.jumpPower);
						this.jump -= 1;
						Input.isKeyPressEdge(this.keymap.up)
					}
				}
				else
					direction.add(new Victor(0, -1));
			}
			if (!this.isAlive && Input.isKeyPress(this.keymap.down)) // Disable when alive
				direction.add(new Victor(0, 1));
			if (Input.isKeyPress(this.keymap.right))
				direction.add(new Victor(1, 0));
			if (Input.isKeyPress(this.keymap.left))
				direction.add(new Victor(-1, 0));
		}

		// Frein Ghost
		if (!this.isAlive && (this.movement.x != 0 || this.movement.y != 0) && (direction.x == 0 || direction.y == 0)) {
			var frein = new Victor(this.movement.x, this.movement.y).invert().norm().multiply(new Victor(freinCoef * el, freinCoef * el));
		
			if (direction.x == 0 && direction.y == 0) {
				if (frein.lengthSq() > this.movement.lengthSq())
					super.stopMouvement();
				else
					super.updateMovement(frein);
			}
			else if (direction.x == 0) {
				if (Math.abs(frein.x) > Math.abs(this.movement.x))
					this.movement = new Victor(0, this.movement.y);
				else
					this.movement.addX(frein);
			}
			else {
				if (Math.abs(frein.y) > Math.abs(this.movement.y))
					this.movement = new Victor(this.movement.x, 0);
				else
					this.movement.addY(frein);
			}
		}

		// Accelerate
		if (direction.x != 0 || direction.y != 0)
			super.updateMovement(direction.norm().multiply(new Victor(speedCoef * el, speedCoef * el)));
		// Frein Rabbit
		else if (this.isAlive)
			super.updateMovement(new Victor((Math.abs(this.movement.x) > 2 ? (this.movement.x / Math.abs(this.movement.x)) * el * freinCoef * -1 : -this.movement.x), 0));

		// Gravity + Limite Speed on X
		if (this.isAlive) {
			super.updateMovement(new Victor(0, el * GameScene.gravity));
			if (Math.abs(this.movement.x) > this.maxSpeed)
				this.movement = new Victor(this.maxSpeed * (this.movement.x / Math.abs(this.movement.x)), this.movement.y);
		}

		// Select Anim
		if (!this.spellActive) {
			var anim = null;
			if (this.isAlive) {
				if (!(-1 < this.movement.x && this.movement.x < 1)) {
					this.changeAnim(GameScene.sprites.player.makeAnim("run"));
				}
				else {
					this.changeAnim(GameScene.sprites.player.makeAnim("idle"));
				}
			}
			else
				this.changeAnim(GameScene.sprites.monsters[GameScene.sprites.monstersTranslate[this.id]].makeAnim("idle"));
		}

		// Set Sprite Direction
		if (this.isFaceRight && direction.x < 0 && this.anim.scale.x > 0) {
			this.isFaceRight = false;
			this.anim.anchor.x = 1;
			this.anim.scale.x *= -1;
		}
		else if (!this.isFaceRight && direction.x > 0 && this.anim.scale.x < 0) {
			this.isFaceRight = true;
			this.anim.anchor.x = 0;
			this.anim.scale.x = Math.abs(this.anim.scale.x);
		}

		// Divide Speed if in take over
		if (this.contactToOtherSide > 0)
			this.movement.divide(GameScene.takeOverReductor);

		// Run all spells
		this.spells.forEach(function(spell) {
			if (spell.playerLifeStateRequired === this.isAlive) {
				if (!this.spellActive && Input.isKeyPress(spell.getKey()))
					spell.doTheSpell(this);
				spell.update(el);
			}				
		}, this);

		super.run(elapsed);

		// Reset Speed for calculation !== Opti over 9000
		if (this.contactToOtherSide > 0)
			this.movement.multiply(GameScene.takeOverReductor);

		// Center the player name
		if (typeof this.title !== 'undefined')
			this.title.position.x = (this.anim.width - this.title.width) / 2;
	}

	onCollisionStart(obj, typeX, typeY, deltaX, deltaY) {
		if (typeY == -1 && (deltaX > this.width / 4 || obj.width == deltaX) && !obj.isMoving())
			this.jump = 1;

		if (obj instanceof Player) {
			if (obj.isAlive != this.isAlive)
				this.contactToOtherSide++;
			if (obj.isAlive) {
				this.takeOverBar.resizeBacgroud(this.anim.width);
				this.takeOverBar.updateValue(0);
				this.takeOverBar.stage.visible = true;
				//this.takeOverBar.style.display = "block";
				this.takeOverdir = 1;
			}
		}
	}

	onCollision(obj, typeX, typeY, deltaX, deltaY) {
		if (obj instanceof Player && obj.isAlive && this.takeOver > GameScene.takeOverTime) {
			obj.setAlive(false);

			this.resetTakeOver();
			this.setAlive(true);
			this.contactToOtherSide = 0;

			// Send away BadGuy
			this.collideWith.forEach(function(element) {
				if (obj instanceof Player && !obj.isAlive) {
					element.movement = new Victor(Math.random() * 2 - 1, Math.random() * 2 - 1).normalize().multiply(new Victor(GameScene.phatomMaxSpeed, GameScene.phatomMaxSpeed));
					element.resetTakeOver();
					element.contactToOtherSide = 0;
				}
			});
		}
	}

	onCollisionStop(obj) {
		if (!obj.isMoving() && Math.abs((obj.x + obj.width / 2) - (this.x + this.width / 2)) * 2 < (obj.width + this.width) && this.y + this.height < obj.y)
			this.jump += 1;

		if (obj instanceof Player) {
			if (obj.isAlive != this.isAlive && this.contactToOtherSide > 0)
				this.contactToOtherSide--;
			if (obj.isAlive)
				this.takeOverdir = -1;
		}
	}

	setSize(width, height) {
		this.width = width;
		this.height = height;

		return this;
	}

	setName(playerName, color) {
		//var tmp = this.html.querySelector(".name");
		//if (tmp == null) {
		//	tmp = document.createElement("div");
		//	tmp.classList.add("name");
		//	$(this.html).append(tmp);
		//}

		//if (typeof color != 'undefined')
		//	tmp.style.color = color;

		//tmp.innerHTML = playerName;

		if (typeof this.title === 'undefined') {
			this.title = new PIXI.Text(playerName, {fontFamily : 'MotionControl-Bold', fontSize: 20, fill : 0xff1010, align : 'center', strokeThickness: 2, stroke: 0x000000});
			this.title.position.y = -this.title.height;
			this.metaDisplay.addChild(this.title);
		}

		this.title.setText(playerName);
		this.title.style.fill = color;
		this.name = playerName;
		return this;
	}
}