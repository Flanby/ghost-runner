
class Exit extends ObjectDisplay {
    constructor(x, y) {
        super();

        var text = PIXI.loader.resources[rootDir+"Assets/Map/Exit.png"].texture;

        this.setPos(x * Block.size + (Block.size - text.orig.width) / 2, y * Block.size + (Block.size - text.orig.height)).
            setCollideMask(GameScene.mask.trigger.player).
            setSize(text.orig.width, text.orig.height);

        this.anim = new PIXI.Sprite(text);
    }

	onCollisionStart(obj, typeX, typeY, deltaX, deltaY) {
        GameScene.setWinner(obj);
		return ;
	}
}