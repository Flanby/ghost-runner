
class Block extends ObjectDisplay {
    constructor(ressource, width, height, x, y) {
        super();

        this.setPos(x * Block.size, y * Block.size).
            setCollideMask(GameScene.mask.trigger.player | GameScene.mask.collide.player).
            setSize(width * Block.size, height * Block.size);

        this.anim = new PIXI.Container();
        var a = null, sizey = height < 1 ? height * Block.size : Block.size;
        for(var j = 0; j < height; j++)
            for (var i = 0; i < width; i++) {
                a = new PIXI.Sprite(PIXI.loader.resources[rootDir+"Assets/Map/Map.json"].textures[ressource[i+j*width]]);
                a.width = Block.size;
                a.height = sizey;
                a.position.set(i * Block.size, j * Block.size);

                this.anim.addChild(a);
            }
    }

    clone() {
        var clone = super.clone();

        clone.anim = new PIXI.Container();
        this.anim.children.forEach(function(text) {
            var sprite = new PIXI.Sprite(text.texture.clone());
            sprite.position.set(text.x, text.y);
            sprite.scale.set(text.scale.x, text.scale.y);
            clone.anim.addChild(sprite);
        });

        return clone;
    }
}

Block.size = 100;