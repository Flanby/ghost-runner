
class AI extends Player {
    constructor(id, mainStage = null) {
        super(id, mainStage);
        this.isIA = true;
    }

    run(elapsed) {
        this.AIdirection = new Victor(0, 0);

        if (this.isAlive) {

        }
        else {
            for (var i = 0; i < GameScene.players.length; i++) {
                if (GameScene.players[i].isAlive) {
                    var target = GameScene.players[i];

                    this.AIdirection.x = target.x - this.x;
                    this.AIdirection.y = target.y - this.y;

                    var norm = this.AIdirection.length(); // == Math.sqrt(this.AIdirection.x * this.AIdirection.x + this.AIdirection.y * this.AIdirection.y);

                    this.AIdirection = this.AIdirection.norm();


                    if (norm < 500) {
                        var coefFrein = (500 - norm) / 500 * -1;

                        // Add prevision movement alive player + random ?
                        this.AIdirection = this.movement.clone().norm().multiply(new Victor(coefFrein, coefFrein)).add(this.AIdirection).norm();
                    }

                    if (isNaN(this.AIdirection.length()))
                        this.AIdirection = new Victor(0, 0);

                    break ;
                }
            }
            
        }

        super.run(elapsed);
    }

}