
class MovingObject extends ObjectDisplay {
	constructor() {
		super();
		this.movement = new Victor(0, 0);
		this.setMaxSpeed(GameScene.playerMaxSpeed);
		this.moving = true;
		this.controlSpeed = true;
	}

	setMaxSpeed(speed) {
		this.maxSpeed = speed;
		this.maxSpeedSq = Math.pow(speed, 2);
	}

	updateMovement(up) {
		this.movement.add(up);
	}

	setMouvement(speed) {
		this.movement = speed;
	}

	stopMouvement() {
		this.movement = new Victor(0, 0);
	}

	updatePos(x, y) {
		super.setPos(this.x + x, this.y + y);
	}

	run(elapsed) {
		if (this.controlSpeed && this.movement.lengthSq() > this.maxSpeedSq)
			this.movement.norm().multiply(new Victor(this.maxSpeed, this.maxSpeed))

		elapsed /= 100.0;

		this.updatePos(Math.round(this.movement.x * elapsed), Math.round(this.movement.y * elapsed));
	}
};