
class Bar {
    constructor(width, len, color, direction = Bar.UpToDown) {
        this.direction = direction;
        
        this.len = len;
        this.width = width;

        this.stage = new PIXI.Container();
        
        this.bar = new PIXI.Graphics();

        this.bar.beginFill(color);
        if (this.direction & 0b10)
            this.bar.drawRect(0, 0, len, width); // Horizontal
        else
            this.bar.drawRect(0, 0, width, len); // Vertical
        this.bar.endFill();

        this.color = color;
        
        this.stage.addChild(this.bar);
        this.border = null;
    }

    drawBorder() {
        this.border.beginFill(this.borderColor);
        if (this.direction & 0b10)
            this.border.drawRect(0, 0, this.len + this.borderSize * 2, this.width + this.borderSize * 2); // Horizontal
        else
            this.border.drawRect(0, 0, this.width + this.borderSize * 2, this.len + this.borderSize * 2); // Vertical
        this.border.endFill();
    }

    addBorder(size, color) {
        this.border = new PIXI.Graphics();
        this.borderSize = size;
        this.borderColor = color;

        this.drawBorder();

        this.bar.clear();
        this.bar.beginFill(this.color);
        if (this.direction & 0b10)
            this.bar.drawRect(size, size, this.len, this.width); // Horizontal
        else
            this.bar.drawRect(size, size, this.width, this.len); // Vertical
        this.bar.endFill();

        this.stage.addChildAt(this.border, 0);
        
        return this;
    }

    resize(len, width = this.width) {
        if (len == this.len && width == this.width)
            return;
        this.len = len;
        this.width = width;
        
        this.updateValue(1);

        if (this.border != null) {
            this.border.clear();
            this.drawBorder();
        }
    }

    resizeBacgroud(len, width = this.width) {
        if (len == this.len && width == this.width)
            return;
        
        this.len = len;
        this.width = width;

        if (this.border != null) {
            this.border.clear();
            this.drawBorder();
        }
    }

    updateValue(percentage) {
        var lenbar = percentage * this.len, rest = this.len - lenbar;

        this.bar.clear();
        this.bar.beginFill(this.color);

        if (this.direction == Bar.UpToDown)
            this.bar.drawRect(this.borderSize, this.borderSize + rest, this.width, lenbar); //this.bar.moveTo(this.bar.x, rest);
        else if (this.direction == Bar.DownToUp)
            this.bar.drawRect(this.borderSize, this.borderSize, this.width, lenbar);
        else if (this.direction == Bar.LeftToRight)
            this.bar.drawRect(this.borderSize, this.borderSize, lenbar, this.width);
        else if (this.direction == Bar.RightToLeft)
            this.bar.drawRect(this.borderSize + rest, this.borderSize, lenbar, this.width);
        this.bar.endFill();
    }

    getBarDisplay() {
        return this.stage;
    }
}

Bar.UpToDown = 0;
Bar.DownToUp = 1;
Bar.LeftToRight = 2;
Bar.RightToLeft = 3;