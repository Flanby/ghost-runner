// Mask / 1 triger player, 2 triger ghost ,3 ??, 4 ??, 5 bloque player, 6 bloque monstre
class ObjectDisplay extends BaseObject {

	constructor() {
		super();

		//this.previousClass = null;

		//this.html = document.createElement("div");
		//this.html.classList.add("object");

		this.anim = null;
	}

	setSize(width, height) {
		this.width = width;
		this.height = height;

		if (this.anim != null) {
			this.anim.height = height;
			this.anim.width = width;
		}

		return this;
	}

	setAnim(anim) {
		this.anim = anim;
		return this;
	}

	getAnim() {
		return this.anim;
	}

	updateDisplay(xOffset = 0, yOffset = 0) {
		//if (this.moving) {
			this.anim.x = (this.x - xOffset);
			this.anim.y = (this.y - yOffset);
		//}
	}

	clone() {
		var clone = super.clone();
		//clone.html = clone.html.cloneNode(true);
		//clone.anim = Object.setPrototypeOf(Object.assign({}, this.anim), this.anim.__proto__)
		return clone;
	}

};