
class BaseObject {

	constructor() {
		this.x = 0;
		this.y = 0;

		this.height = 0;
		this.width = 0;

		this.collideMask = 0b00110011;
		this.collideWith = [];
		this.collideWithPrev = [];

		this.moving = false;
	}

	isMoving() {
		return this.moving;
	}

	setCollideMask(mask) {
		this.collideMask = mask;
		return this;
	}

	setPos(x, y) {
		this.x = x;
		this.y = y;
		return this;
	}

	setSize(width, height) {
		this.width = width;
		this.height = height;

		return this;
	}

	run(elapsed) {
		return ;
	}

	onCollisionStart(obj, typeX, typeY, deltaX, deltaY) {
		return ;
	}

	onCollision(obj, typeX, typeY, deltaX, deltaY) {
		return ;
	}

	onCollisionStop(obj) {
		return ;
	}

	clone() {
		return Object.setPrototypeOf(Object.assign({}, this), this.__proto__);
	}
};