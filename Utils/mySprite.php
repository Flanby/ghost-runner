<?php
    function display_sprite($folder) {
        $files = array_filter(scandir($folder), function($data) { return preg_match("/\\.(png|jpe?g|gif)$/i", $data); });

        $output = [];
        foreach ($files as $file)
            $output[] = $folder.$file;
        return $output;
    }

    function imagecreatefromjpg($path) {
        return imagecreatefromjpeg($path);
    }

    function imagecreatealpha($width, $height)
    {
        $img = imagecreatetruecolor($width, $height);
        imagealphablending($img, false);
        imagesavealpha($img, true);
        
        $trans = imagecolorallocatealpha($img, 0, 0, 0, 127);
        for ($x = 0; $x < $width; $x++)
            for ($y = 0; $y < $height; $y++)
                imagesetpixel($img, $x, $y, $trans);
        
        return $img;
    }

    $row = ceil(sqrt($nb = count($tiles = array_merge(
        display_sprite("../Assets/Map/Forest/Tiles/"), 
        display_sprite("../Assets/Map/Desert/Tiles/"), 
        display_sprite("../Assets/Map/Graveyard/Tiles/"), 
        display_sprite("../Assets/Map/Winter/Tiles/"), 
        display_sprite("../Assets/Map/Sci-fi/Tiles/")
    ))));

    $blocsize = 100;
    $size = $row * $blocsize;
    $json = array(
        "frames" => array(), 
        "meta" => array(
            "app" => "http://flan-chan.fr/",
            "version" => "1.0",
            "image" => "Map.png",
            "format" => "RGBA8888",
            "size" => array("w" => $size, "h" => $size),
            "scale" => "1",
            "smartupdate" => ""
        )
    );
    //$image = imagecreatealpha($size, $size);
    $tilesimg = [];

    for ($i = 0; $i < $nb; $i++)
        $tilesimg[] = array("name" => substr($tiles[$i], 10), "img" => $tile = ("imagecreatefrom".strtolower(pathinfo($tiles[$i], PATHINFO_EXTENSION)))($tiles[$i]), "ratio" => ($sizey = imagesy($tile)) / $sizex = imagesx($tile), "width" => $sizex, "height" => $sizey, "area" => $sizex * $sizey);
        //imagecopyresized($image, $tile = ("imagecreatefrom".strtolower(pathinfo($tiles[$i], PATHINFO_EXTENSION)))($tiles[$i]), $x = $i % $row * $blocsize, $y = floor($i / $row) * $blocsize, 0, 0, $blocsize, $h = round($blocsize * ($sizey = imagesy($tile)) / $sizex = imagesx($tile)), $sizex, $sizey);
 
    $byArea = [];
    foreach ($tilesimg as $tile) {
        if (!isset($byArea[$tile["area"]]))
            $byArea[$tile["area"]] = array();
        $byArea[$tile["area"]][] = $tile;
    }
    ksort($byArea);
    $byArea = array_reverse($byArea, true);

    $row = ceil(sqrt(count(reset($byArea))));
    
    $width = $row * reset($byArea)[0]["width"];
    $height = 0;
    foreach ($byArea as $area)
        $height += ceil(count($area) / floor($width / $area[0]["width"])) * $area[0]["height"];

    function addTile(&$image, $tile, $realwith, $realheight, &$json) {
        imagecopy($image, $tile["img"], $realwith, $realheight, 0, 0, $tile["width"], $tile["height"]);

        $json["frames"][$tile["name"]] = array(
            "filename" => $tile["name"],
            "frame" => array("x" => $realwith + 1, "y" => $realheight + 1, "w" => $tile["width"] - 2, "h" => $tile["height"] - 2),
            "rotated" => false,
            "trimmed" => false,
            "spriteSourceSize" => array("x" => 0, "y" => 0, "w" => $tile["width"] - 2, "h" => $tile["height"] - 2),
            "sourceSize" => array("w" => $tile["width"] - 2, "h" => $tile["height"] - 2),
            "pivot" => array("x" => 0.5, "y" => 0.5)
        );
    }

    do {
        $json["frames"] = [];
        $realwith = 0;
        $realheight = 0;
        if (!empty($image))
            imagedestroy($image);
        $image = imagecreatealpha($width, $height);

        $maxheight = 0;
        foreach ($byArea as $area) {
            $inarow = floor($width / $area[0]["width"]);
            $stackoffset = 0;
            for ($i = 0; $i < count($area); $i++) {
                if (($i - $stackoffset) % $inarow == 0) {

                    // Make some recursive function to fill remaning space
                    if ($realwith + $area[$i]["width"] <= $width && $area[$i]["height"] <= $maxheight) {
                        $maxheight2 = 0;
                        $realwith2 = $realwith;
                        $heightmod = 0;

                        while ($i < count($area) && $heightmod + $area[$i]["height"] <= $maxheight && $realwith2 + $area[$i]["width"] <= $width) {
                            addTile($image, $area[$i], $realwith2, $realheight + $heightmod, $json);
                            
                            $realwith2 += $area[$i]["width"];
                            $maxheight2 = max($maxheight2, $area[$i]["height"]);
                            if ($realwith2 + $area[$i]["width"] > $width) {
                                $realwith2 = $realwith;
                                $heightmod += $area[$i]["height"];
                                $maxheight2 = 0;
                            }

                            $i++;
                            $stackoffset++;
                        }

                    }
                    $realheight += $maxheight;
                    $realwith = 0;
                    $maxheight = 0;
                }

                if ($i < count($area)) {
                    addTile($image, $area[$i], $realwith, $realheight, $json);

                    $maxheight = max($maxheight, $area[$i]["height"]);
                    $realwith += $area[$i]["width"];
                }
            }
        }
        // Compute the image before building to avoid rebuild it
        $realheight += $maxheight;
        if ($realheight != $height) {
            $height = $realheight;
            continue;
        }
        break;
    } while (1);



    //header('Content-Type: image/png');

    imagepng($image, $json["meta"]["image"]);
    imagedestroy($image);
    file_put_contents("Map.json", json_encode($json));//, JSON_PRETTY_PRINT));
?>