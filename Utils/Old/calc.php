
<?php
	function lol($name, $x, $y)
	{
		$aire = 5250;

		$ratio = $x / $y;

		$y = sqrt($aire/$ratio);
		$x = $y * $ratio;

		$y = round($y);
		$x = round($x);

		$size = $x *8;

		$class = str_replace("_", "-", strtolower($name));

		echo "
.$class {
	background: url(./Assets/Monsters/$name.png);
	background-size: ".$size."px ".$y."px;
	width: ".$x."px;
	height: ".$y."px;
	animation: $class-anim 0.4s steps(8) infinite;
}

@keyframes $class-anim {
    100% { background-position: -$size; }
}

.$class-big {
	background: url(./Assets/Monsters/$name.png);
	background-size: ".($size*2)."px ".($y*2)."px;
	width: ".($x*2)."px;
	height: ".($y*2)."px;
	animation: $class-big-anim 0.4s steps(8) infinite;
}

@keyframes $class-big-anim {
    100% { background-position: -".($size*2)."; }
}

";

	}

	lol("Eye_Monster", 968, 934);
	lol("Ghost_Monster", 705, 904);
	lol("Bomb_Monster", 825, 897);
	lol("Yellow_Monster", 876, 868);
	lol("Moskito", 1172, 882);
	lol("Red_Skull", 906, 898);
	lol("Bat", 1126, 860);
	lol("Dark_Bat", 884, 780);