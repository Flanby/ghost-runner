
<?php

    $dir = scandir("./MapChunks");
    $string = "[\n";
    foreach($dir as $file) {
        if (in_array($file, array(".", "..")) || ($data = parse_ini_file("./MapChunks/".$file)) === false)
            continue;

        $dataNew = array(
            "id" => $data["ID"], 
            "name" => substr($file, 3, -3), 
            "pattern" => array(
                $data["0"], 
                $data["1"], 
                $data["2"], 
                $data["3"], 
                $data["4"]),
            "ChanceOfSpawn" => $data["ChanceOfSpawn"],
            "Difficulty" => $data["Difficulty"],
            "nextTo" => explode(",", $data["nextTo"])
        );

        $string .= "
	{
       \"id\":".$data["ID"].",
        \"name\":\"".substr($file, 3, -3)."\",
        \"pattern\":[
            [".implode(",",str_split(strtr($data["0"], "BESD", "2000")))."],
            [".implode(",",str_split(strtr($data["1"], "BESD", "2000")))."],
            [".implode(",",str_split(strtr($data["2"], "BESD", "2000")))."],
            [".implode(",",str_split(strtr($data["3"], "BESD", "2000")))."],
            [".implode(",",str_split(strtr($data["4"], "BESD", "2000")))."]
        ],
        \"ChanceOfSpawn\":".$data["ChanceOfSpawn"].",
        \"Difficulty\":".$data["Difficulty"].",
        \"nextTo\":".(($arr = explode(",", $data["nextTo"])) === FALSE ? "[]" : json_encode($arr))."
    },";

        //$string .= "\t".json_encode($dataNew).",\n";
    }
    $string .= "
]";

    echo $string;

?>
