<?php
    function display_sprite($folder) {
        $files = array_filter(scandir($folder), function($data) { return preg_match("/\\.(png|jpe?g|gif)$/i", $data); });

        $output = "<h2>$folder</h2>";
        foreach ($files as $file)
            $output .= "<img alt=\"\" src=\"".$folder.$file."\" class=\"bloc\" />";
        return $output;
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Chunk Map Editor: Ghost Runner</title>

    <script src="https://use.fontawesome.com/c37dbf44b3.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.11.0/styles/rainbow.min.css"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.11.0/highlight.min.js"></script>
    <script src="JS/script.js"></script>
    <link rel="stylesheet" href="Css/style.css"/>

</head>
<body>

    <div class="col1 box">
        <div class="content">
            <h1><a href="../../"><i class="fa fa-arrow-circle-o-left" style="margin-right: 10px;"></i></a>Ghost Runner</h1>

            <div class="btn-group">
                <button class="btn btn-success" onclick="AddRowTop()">Row Top</button>
                <button class="btn btn-success" onclick="AddRowBottom()">Row Bottom</button>
            </div>
            <div class="btn-group">
                <button class="btn btn-primary" onclick="AddColLeft()">Col Left</button>
                <button class="btn btn-primary" onclick="AddColRight()">Col Right</button>
            </div>
            <button class="btn btn-danger" onclick="SelectDelete()">Delete</button>
            <button class="btn btn-warning" onclick="getCode()">Code</button>
        </div>

        <div class="content" style="background:inherit">
            <h3 class="fancy"><span onclick="$('#meta').slideToggle()">Map Data</span></h3>
            <div id="meta">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name"/>
                </div>
                <div class="form-group">
                    <label for="spwn">Chance Of Spawn:</label>
                    <input type="text" class="form-control" id="spwn" value="100"/>
                </div>
                <div class="form-group">
                    <label for="diff">Difficulty:</label>
                    <input type="text" class="form-control" id="diff" value="5.0"/>
                </div>
                <div class="form-group">
                    <label for="off">Row Offset:</label>
                    <input type="text" class="form-control" id="off" value="0"/>
                </div>
            </div>
            <hr/>
        </div>

        <div class="fill">
            <?= display_sprite("../../Assets/Map/Forest/Tiles/"); ?>
            <?= display_sprite("../../Assets/Map/Desert/Tiles/"); ?>
            <?= display_sprite("../../Assets/Map/Graveyard/Tiles/"); ?>
            <?= display_sprite("../../Assets/Map/Winter/Tiles/"); ?>
            <?= display_sprite("../../Assets/Map/Sci-fi/Tiles/"); ?>
        </div>
    </div>
    <div class="col2">
        <table>
            <tbody>
                <tr><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td><td></td></tr>
            </tbody>
        </table>

        <div class="code" style="display:none;">
            <button class="btn" onclick="goBack()" style="float: left; margin-bottom: 20px;">
                <i class="fa fa-arrow-circle-o-left" style="margin-right: 10px;"></i>
                Back To Design
            </button>
            <div class="end"></div>
            <pre><code class="js"></code></pre>
        </div>
    </div>

    <div class="end"></div>
</body>
</html>